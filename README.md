ptracking
=========

# Requirements

ptracking requires the following packages to build:
  
  * build-essential
  * g++
  * cmake
  * libxml2
  * boost
  * opencv
  * gnuplot

while this requirement is optional to build the Kinect section of PTaLer:

  * libfreenect (https://github.com/fabioprev/libfreenect.git - a request for getting
				 access is required: previtali@dis.uniroma1.it)

while this requirement is optional to build the Xtion section of PTaLer:

  * OpenNI2

while this requirement is optional to build the ImageDetection section of PTaLer:

  * Imbs (https://github.com/fabioprev/Imbs.git)

while these requirements are mandatory if and only if, you are running a
64-bit Linux distribution but the library have to be compiled for a 32-bit
Linux distribution:
  
  * libc6-dev-i386
  * libgmp3-dev

On Xubuntu (Ubuntu) 14.04 LTS (kernel 3.13.0-37), these dependencies are
resolved by installing the following packages:
  
  - build-essential
  - cmake
  - libxml2
  - libxml2-dev
  - libboost1.54-all-dev
  - libopencv-dev
  - gnuplot
  - gnuplot-x11
  - libopenni2-dev (optional)
  - libc6-dev-i386 (32-bit Linux distribution)
  - libgmp3-dev (32-bit Linux distribution)

and building the following ones (if needed):

  - libfreenect
  - Imbs

Maybe to install the OpenNI2 library, you need to add these lines at the end of the
/etc/apt/sources.list file:

  * ## OpenNI2
  * deb http://ppa.launchpad.net/v-launchpad-jochen-sprickerhof-de/pcl/ubuntu trusty main
  * deb-src http://ppa.launchpad.net/v-launchpad-jochen-sprickerhof-de/pcl/ubuntu trusty main

and then typing in a terminal:

  * sudo apt-get update

# How to build

The only development platform is Linux. We recommend a so-called out of source
build which can be achieved by the following command sequence:
  
  - mkdir build
  - cd build
  - cmake ../src
  - make -j\<number-of-cores+1\>

# Installation

Once the build phase has been successfully, the library have to be installed
so that it can be linked in other projects. This is achieved by the
following command sequence:
  
  - cd build
  - sudo make install

After the installation the header files have been copied to /usr/local/include/PTracking,
while the libptracking.so shared object has been copied to /usr/local/lib/PTracking.

The executables:
  
  * PTaLer
  * PTracker
  * PViewer
  * PVisualizer

have been copied to /usr/local/bin.

The last step before correctly using the library is to logout because the file ~/.profile
has been modified by the installation.

# Usage

Before executing any program, you need to verify in the parameters.cfg file whether the
filtering parameters are suitable for the chosen environment.
  
  * To execute PTaLer you need to have either a set of images or a live sensor (e.g., Kinect or Xtion)
    or an observation file and type in to a terminal one of the following command:
    
        * PTaLer <problem-file> <frame-rate> -img <first-image-file>
          [ -loadbg <background-file> -calib <calibration-directory>
          -agentId <agent-number> -pause ]
		
        * PTaLer <problem-file> <frame-rate> -s <sensor-type>
          [ -loadbg <background-file> -calib <calibration-directory>
          -agentId <agent-number> -pause ]

        * PTaLer <problem-file> <frame-rate> -obs <observation-file>
  
  * To execute the tracking algorithm you just need to have an observation file and type in to a
    terminal the following command:
    
    - PTracker \<path-observation-file\>
  
  * To visualize in Gnuplot the tracking data generated by PTracker type in to another terminal:
    
    - PViewer
