/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Core/Filters/ObjectSensorReading.h>

/**
 * @class PVisualizer
 * 
 * @brief Class that implements a tool for visualizing all trajectories in a data set in a xml format.
 */
class PVisualizer
{
	private:
		/**
		 * @brief map containing the mapping between identity and color.
		 */
		std::map<int,std::pair<int,std::pair<int,int> > > colorMap;
		
		/**
		 * @brief Function that allows a clean exit intercepting the SIGINT signal.
		 */
		static void interruptCallback(int);
		
		/**
		 * @brief Function that reads the xml file containing all the estimations performed by PTracker.
		 * 
		 * @param estimationFile reference to the file containing all the estimations.
		 * 
		 * @return the data structure containing all the estimations.
		 */
		std::vector<std::pair<std::vector<int>,PTracking::ObjectSensorReading> > readEstimationFile(const std::string& estimationFile);
		
	public:
		/**
		 * @brief Empty constructor.
		 * 
		 * It registrers the SIGINT callback.
		 */
		PVisualizer();
		
		/**
		 * @brief Destructor.
		 */
		~PVisualizer();
		
		/**
		 * @brief Function that visualizes the PTracker results in the images using OpenCV. It stops when either has finished or Ctrl+C is pressed.
		 * 
		 * @param imagePath reference to the dataset image path.
		 * @param resultsXmlFile reference to the results xml file.
		 */
		void exec(const std::string& imagePath, const std::string& resultsXmlFile);
};
