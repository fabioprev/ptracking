/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Core/Filters/ObjectSensorReading.h>
#include <opencv2/core/core.hpp>

namespace Detectors
{
	/**
	 * @class BasicDetector
	 * 
	 * @brief Abstract class that defines an interface for a detector algorithm.
	 */
	class BasicDetector
	{
		public:
			/**
			 * @brief Destructor.
			 */
			virtual ~BasicDetector() {;}
			
			/**
			 * @brief Function that detects object in the frame.
			 * 
			 * @param frame reference to the current frame to analyze.
			 * 
			 * @return the objects detected in the current frame.
			 */
			virtual PTracking::ObjectSensorReading exec(const cv::Mat& frame) = 0;
			
			/**
			 * @brief Function that checks whether the background has been created, if overloaded.
			 * 
			 * @return \b true if the background has been created correctly, \b false otherwise.
			 */
			virtual bool isBackgroundCreated() { return false; }
			
			/**
			 * @brief Function that loads a background, if overloaded.
			 * 
			 * @param filename file containing the background data.
			 * 
			 * @return \b true if the background has been loaded correctly, \b false otherwise.
			 */
			virtual bool loadBg(const std::string& filename) { return false; }
			
			/**
			 * @brief Function that sets several parameters for the detection, if overloaded.
			 * 
			 * @param filename file to be read.
			 * @param agentId id of the agent.
			 * @param calibrationDirectory directory containing the calibration files.
			 */
			virtual void setDetectorParameters(const std::string& filename, int agentId, const std::string& calibrationDirectory) {;}
	};
}
