/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "HOG.h"

using namespace std;
using namespace cv;
using namespace PTracking;

namespace Detectors
{
	HOG::HOG(float frameRate) : frameRate(frameRate)
	{
		hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());
	}
	
	HOG::~HOG() {;}
	
	ObjectSensorReading HOG::exec(const Mat& frame)
	{
		vector<ObjectSensorReading::Observation> observations;
		ObjectSensorReading visualReadings;
		vector<Rect> found, foundFiltered;
		
		hog.detectMultiScale(frame,found,0,Size(8,8),Size(32,32),1.05,2);
		
		for (vector<Rect>::const_iterator it = found.begin(); it != found.end(); ++it)
		{
			vector<Rect>::const_iterator it2 = found.begin();
			
			for (; it2 != found.end(); ++it2)
			{
				if (it != it2 && (*it & *it2) == *it) break;
			}
			
			if (it2 == found.end()) foundFiltered.push_back(*it);
		}
		
		for (vector<Rect>::const_iterator it = foundFiltered.begin(); it != foundFiltered.end(); ++it)
		{
			ObjectSensorReading::Observation o;
			
			o.observation.x = it->x + it->width / 2;
			o.observation.y = it->y + it->height;
			
			o.head.x = it->x + it->width / 2;
			o.head.y = it->y;
			
			o.model.width = cvRound(it->width * 0.8);
			o.model.height = cvRound(it->height * 0.9);
			o.model.boundingBox = make_pair(PTracking::Point2f(-o.model.width / 2,-o.model.height),PTracking::Point2f(o.model.width / 2,0));
			o.model.barycenter = o.observation.x;
			
			observations.push_back(o);
		}
		
		visualReadings.setObservations(observations);
		
		return visualReadings;
	}
}
