/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Detectors/BasicDetector.h>
#include <Imbs/Core/Imbs.hpp>
#include <Imbs/Utils/ObservationManager.h>
#include <Imbs/Utils/VisualReading.h>

namespace Detectors
{
	/**
	 * @class Imbs
	 * 
	 * @brief Class that implements a wrapper to the Imbs detector.
	 */
	class Imbs : public BasicDetector
	{
		private:
			/**
			 * @brief detector based on background subtraction.
			 */
			BackgroundSubtractorIMBS imbs;
			
			/**
			 * @brief background of the environment.
			 */
			cv::Mat backgroundImage;
			
			/**
			 * @brief foreground of the environment.
			 */
			cv::Mat foregroundImage;
			
			/**
			 * @brief manager of the observations.
			 */
			ObservationManager observationManager;
			
			/**
			 * @brief bottom right corner of the cropped image.
			 */
			PTracking::Point2f bottomRight;
			
			/**
			 * @brief top left corner of the cropped image.
			 */
			PTracking::Point2f topLeft;
			
			/**
			 * @brief minimum bounding box area for a detection.
			 */
			int minArea;
			
			/**
			 * @brief minimum bounding box height for a detection.
			 */
			int minHeight;
			
			/**
			 * @brief minimum bounding box width for a detection.
			 */
			int minWidth;
			
			/**
			 * @brief maximum bounding box height for a detection.
			 */
			int maxHeight;
			
			/**
			 * @brief maximum bounding box width for a detection.
			 */
			int maxWidth;
			
			/**
			 * @brief maximum bounding box area for a detection.
			 */
			int maxArea;
			
			/**
			 * @brief true means that cropping images is enabled.
			 */
			bool enabled;
			
			/**
			 * @brief Function that converts the detections in a format readable for PTracker.
			 * 
			 * @param detections reference to the detections to be converted.
			 * 
			 * @return the detections in a format readable for PTracker.
			 */
			PTracking::ObjectSensorReading convertDetectionsForPTracker(const VisualReading& detections) const;
			
		public:
			/**
			 * @brief Constructor that initializes the Imbs detector with the given frame rate.
			 * 
			 * @param frameRate number of frames per second.
			 */
			Imbs(float frameRate);
			
			/**
			 * @brief Destructor.
			 */
			~Imbs();
			
			/**
			 * @brief Function that detects object in the frame using the background subtraction algorithm.
			 * 
			 * @param frame reference to the current frame to analyze.
			 * 
			 * @return the objects detected in the current frame.
			 */
			PTracking::ObjectSensorReading exec(const cv::Mat& frame);
			
			/**
			 * @brief Function that returns the background image of the environment.
			 * 
			 * @return the background image of the environment.
			 */
			inline const cv::Mat& getBackgroundImage() const { return backgroundImage; }
			
			/**
			 * @brief Function that returns the foreground image of the environment.
			 * 
			 * @return the foreground image of the environment.
			 */
			inline const cv::Mat& getForegroundImage() const { return foregroundImage; }
			
			/**
			 * @brief Function that checks whether the background has been generated.
			 * 
			 * @return \b true if the background has been generated correctly, \b false otherwise.
			 */
			inline bool isBackgroundCreated() { return imbs.isBackgroundCreated; }
			
			/**
			 * @brief Function that loads a background.
			 * 
			 * @param filename file containing the background data.
			 * 
			 * @return \b true if the background has been loaded correctly, \b false otherwise.
			 */
			bool loadBg(const std::string& filename);
			
			/**
			 * @brief Function that saves the background data.
			 * 
			 * @param filename name of the file in which the background has to be saved.
			 */
			void saveBg(std::string& filename);
			
			/**
			 * @brief Function that sets several parameters for the detection.
			 * 
			 * @param filename file to be read.
			 * @param agentId id of the agent.
			 * @param calibrationDirectory directory containing the calibration files.
			 */
			void setDetectorParameters(const std::string& filename, int agentId, const std::string& calibrationDirectory);
	};
}
