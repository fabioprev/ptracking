/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "PTracker.h"
#include <Core/Sensors/BasicSensor.h>
#include <Manfield/ConfigFile/ConfigFile.h>
#include <Utils/UdpSocket.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <algorithm>
#include <ctime>
#include <iomanip>

/// Uncomment to enable debug prints.
//#define DEBUG_MODE ;

using namespace std;
using namespace cv;
using namespace PTracking;
using GMapping::ConfigFile;

PTracker::PTracker(float frameRate) : H(3,3,DataType<float>::type), Hinv(3,3,DataType<float>::type), frameRate(frameRate), agentId(-1)
{
	signal(SIGINT,PTracker::interruptCallback);
	
	init(frameRate);
	
	pthread_t waitAgentMessagesThreadId;
	
	pthread_create(&waitAgentMessagesThreadId,0,(void*(*)(void*)) waitAgentMessagesThread,this);
}

PTracker::PTracker(int agentId, const string& filename, const string& calibrationDirectory, float frameRate) : H(3,3,DataType<float>::type), Hinv(3,3,DataType<float>::type), frameRate(frameRate), agentId(agentId)
{
	signal(SIGINT,PTracker::interruptCallback);
	
	init(frameRate,filename,calibrationDirectory);
	
	pthread_t waitAgentMessagesThreadId;
	
	pthread_create(&waitAgentMessagesThreadId,0,(void*(*)(void*)) waitAgentMessagesThread,this);
}

PTracker::~PTracker() {;}

string PTracker::buildHeader() const
{
	stringstream header;
	int differentTypes;
	
	differentTypes = estimatedTargetModelsMultiAgent.size();
	
	if (abs(currentTargetIndex - lastCurrentTargetIndex) > 0) ++differentTypes;
	
	differentTypes += objectParticleFilter.getObservationsMapping().size();
	
	header << agentId << " " << differentTypes << " ";
	
	/// Pay attention: "true" or "false" means respectively whether the point is oriented or not. In the first case you must use Point2ofOnMap.
	//header << "0" << " " << differentType << " AgentPose true #0000ff " << 7 << " " << 2.0 << " ";
	
	for (MultiAgentEstimations::const_iterator it = estimatedTargetModelsMultiAgent.begin(); it != estimatedTargetModelsMultiAgent.end(); ++it)
	{
		stringstream s;
		
		const map<int,pair<int,pair<int,int> > >::const_iterator& colorTrack = colorMap.find(it->first);
		
		s << "#" << setw(2) << setfill('0') << std::hex << colorTrack->second.second.second
				 << setw(2) << setfill('0') << std::hex << colorTrack->second.second.first
				 << setw(2) << setfill('0') << std::hex << colorTrack->second.first;
		
		header << "EstimatedTargetModelsWithIdentityMultiAgent false " << s.str() << " " << 6 << " " << (1.5 * agentId) << " ";
	}
	
	if (abs(currentTargetIndex - lastCurrentTargetIndex) > 0)
	{
		header << "TargetPerceptions false #0000ff " << 14 << " " << 2;
	}
	
	const map<int,pair<ObjectSensorReading::Observation,Point2of> >& observationsMapping = objectParticleFilter.getObservationsMapping();
	
	for (map<int,pair<ObjectSensorReading::Observation,Point2of> >::const_iterator it = observationsMapping.begin(); it != observationsMapping.end(); ++it)
	{
		header << " ObservationsMapping false #000000 " << 0 << " " <<  2 << " ";
	}
	
	return header.str();
}

SingleAgentEstimations PTracker::calibrate(const SingleAgentEstimations& estimations) const
{
	if (!calibrateData) return estimations;
	
	SingleAgentEstimations calibratedEstimations;
	
	for (SingleAgentEstimations::const_iterator it = estimations.begin(); it != estimations.end(); ++it)
	{
		ObjectSensorReading::Observation calibratedEstimation;
		float imageX, imageY, imageHeadX, imageHeadY, worldX, worldY, worldHeadX, worldHeadY;
		
		imageX = it->second.first.observation.x;
		imageY = it->second.first.observation.y;
		
		imageHeadX = it->second.first.head.x;
		imageHeadY = it->second.first.head.y;
		
		worldX = (H.at<float>(0,0) * imageX + H.at<float>(0,1) * imageY + H.at<float>(0,2)) / (H.at<float>(2,0) * imageX + H.at<float>(2,1) * imageY + H.at<float>(2,2));
		worldY = (H.at<float>(1,0) * imageX + H.at<float>(1,1) * imageY + H.at<float>(1,2)) / (H.at<float>(2,0) * imageX + H.at<float>(2,1) * imageY + H.at<float>(2,2));
		
		worldHeadX = (H.at<float>(0,0) * imageHeadX + H.at<float>(0,1) * imageHeadY + H.at<float>(0,2)) / (H.at<float>(2,0) * imageHeadX + H.at<float>(2,1) * imageHeadY + H.at<float>(2,2));
		worldHeadY = (H.at<float>(1,0) * imageHeadX + H.at<float>(1,1) * imageHeadY + H.at<float>(1,2)) / (H.at<float>(2,0) * imageHeadX + H.at<float>(2,1) * imageHeadY + H.at<float>(2,2));
		
		/// Copying all data that do not change because of the calibration.
		calibratedEstimation = it->second.first;
		
		calibratedEstimation.observation.x = worldX * resolution;
		calibratedEstimation.observation.y = worldY * resolution;
		calibratedEstimation.head.x = worldHeadX * resolution;
		calibratedEstimation.head.y = worldHeadY * resolution;
		calibratedEstimation.model.barycenter = calibratedEstimation.observation.x;
		calibratedEstimation.model.averagedVelocity.x *= resolution;
		calibratedEstimation.model.averagedVelocity.y *= resolution;
		calibratedEstimation.model.velocity.x *= resolution;
		calibratedEstimation.model.velocity.y *= resolution;
		
		calibratedEstimations.insert(make_pair(it->first,make_pair(calibratedEstimation,it->second.second)));
	}
	
	return calibratedEstimations;
}

void PTracker::configure(const string& filename, const string& calibrationDirectory)
{
	vector<int> agentVector;
	ConfigFile fCfg;
	stringstream s;
	string agents, key, section, temp;
	bool isPresent;
	
	if (!fCfg.read(string(getenv("PTracking_ROOT")) + string("/../config/agent.cfg")))
	{
		ERR("Error reading file '" << string(getenv("PTracking_ROOT")) << string("/../config/agent.cfg") << "' for PTracker configuration. Exiting..." << endl);
		
		exit(-1);
	}
	
	try
	{
		section = "parameters";
		
		/// It could have been set by using the constructor.
		if (agentId == -1)
		{
			key = "agentId";
			agentId = fCfg.value(section,key);
		}
		
		key = "agents";
		agents = string(fCfg.value(section,key));
		
		s << agents;
		
		while (s.good())
		{
			string temp;
			
			if (s.eof()) break;
			
			getline(s,temp,',');
			
			agentVector.push_back(atoi(temp.c_str()));
		}
		
		key = "messageFrequency";
		messageFrequency = fCfg.value(section,key);
		
		section = "Agent";
		isPresent = false;
		
		for (vector<int>::const_iterator it = agentVector.begin(); it != agentVector.end(); it++)
		{
			s.str("");
			s.clear();
			
			s << "Agent" << *it;
			
			key = s.str() + "Address";
			const string address = fCfg.value(section,key);
			
			key = s.str() + "Port";
			int p = fCfg.value(section,key);
			
			/// Checking if the agentId is present in the receivers' list. If so, the information between the local and global layer are exchanged by using the main memory.
			if (*it == agentId)
			{
				isPresent = true;
				agentAddress = address;
				agentPort = p;
				
				continue;
			}
			
			WARN("Adding receiver: " << address << ":" << p << endl);
			
			receivers.push_back(make_pair(address,p));
		}
		
		if (!isPresent)
		{
			ERR("The agent id " << agentId << " is not present in the list of the receivers. Please check the configuration! Exiting..." << endl);
			
			exit(-1);
		}
	}
	catch (...)
	{
		ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
		
		exit(-1);
	}
	
	if (!fCfg.read(filename))
	{
		ERR("Error reading file '" << filename << "' for PTracker configuration. Exiting..." << endl);
		
		exit(-1);
	}
	
	try
	{
		section = "parameters";
		
		key = "bestParticles";
		bestParticlesNumber = fCfg.value(section,key);
		
		key = "calibrateData";
		calibrateData = fCfg.value(section,key);
		
		section = "sensor";
		
		key = "maxReading";
		maxReading = fCfg.value(section,key);
		
		section = "location";
		
		key = "worldXMin";
		temp = string(fCfg.value(section,key));
		
		if (temp == "-inf") worldX.x = -FLT_MAX;
		else worldX.x = atof(temp.c_str());
		
		key = "worldXMax";
		temp = string(fCfg.value(section,key));
		
		if (temp == "inf") worldX.y = FLT_MAX;
		else worldX.y = atof(temp.c_str());
		
		key = "worldYMin";
		temp = string(fCfg.value(section,key));
		
		if (temp == "-inf") worldY.x = -FLT_MAX;
		else worldY.x = atof(temp.c_str());
		
		key = "worldYMax";
		temp = string(fCfg.value(section,key));
		
		if (temp == "inf") worldY.y = FLT_MAX;
		else worldY.y = atof(temp.c_str());
	}
	catch (...)
	{
		ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
		
		exit(-1);
	}
	
	if (!fCfg.read(string(getenv("PTracking_ROOT")) + string("/../config/pviewer.cfg")))
	{
		ERR("Error reading file '" << string(getenv("PTracking_ROOT")) << string("/../config/pviewer.cfg") << "' for PTracker configuration. Exiting..." << endl);
		
		exit(-1);
	}
	
	try
	{
		section = "PViewer";
		
		key = "address";
		pViewerAddress = string(fCfg.value(section,key));
		
		key = "port";
		pViewerPort = fCfg.value(section,key);
	}
	catch (...)
	{
		ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
		
		exit(-1);
	}
	
	if (!fCfg.read(string(getenv("PTracking_ROOT")) + string("/../config/rosbridge.cfg")))
	{
		ERR("Error reading file '" << string(getenv("PTracking_ROOT")) << string("/../config/rosbridge.cfg") << "' for PTracker configuration. Exiting..." << endl);
		
		exit(-1);
	}
	
	try
	{
		section = "ROSBridge";
		
		key = "address";
		rosBridgeAddress = string(fCfg.value(section,key));
		
		key = "port";
		rosBridgePort = fCfg.value(section,key);
		
		key = "enabled";
		rosBridgeEnabled = fCfg.value(section,key);
	}
	catch (...)
	{
		ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
		
		exit(-1);
	}
	
	if ((calibrationDirectory != "") && calibrateData) readCalibrationData(calibrationDirectory);
	else if (calibrateData)
	{
		ERR("A calibration of the data has been requested in '");
		WARN(filename);
		ERR("' but a calibration directory has not been provided. Exiting..." << endl);
		
		exit(-1);
	}
}

void PTracker::init(float frameRate, string filename, const string& calibrationDirectory)
{
	int counter;
	
	if (getenv("PTracking_ROOT") == 0)
	{
		ERR("PTracking_ROOT has NOT been set. Did you log out before running any executable of the PTracking library? Exiting..." << endl);
		
		exit(-1);
	}
	
	if (filename == "") filename = string(getenv("PTracking_ROOT")) + string("/../config/parameters.cfg");
	
	configure(filename,calibrationDirectory);
	
	initialTimestamp.setToNow();
	initialTimestampMas.setToNow();
	currentTimestamp.setToNow();
	counterResult = -1;
	currentTargetIndex = 0;
	iterationCounter = 0;
	lastCurrentTargetIndex = 0;
	lastTargetIndex = -1;
	maxTargetIndex = 0;
	
	processor.addSensorFilter(&objectParticleFilter);
	
	objectParticleFilter.configure(filename,frameRate);
	objectParticleFilter.initFromUniform();
	
	processor.init();
	
	multiAgentProcessor.addSensorFilter(&objectParticleFilterMultiAgent);
	
	objectParticleFilterMultiAgent.configure(filename,agentPort,frameRate,calibrationDirectory,agentId);
	objectParticleFilterMultiAgent.initFromUniform();
	
	multiAgentProcessor.init();
	
	objectSensorReading.setSensor(objectParticleFilter.getSensor());
	
	srand(time(0));
	
	counter = 1;
	
	for (int i = 0; i < 256; i += 63)
	{
		for (int j = 0; j < 256; j += 63)
		{
			for (int k = 0; k < 256; k += 128, ++counter)
			{
				colorMap.insert(make_pair(counter,make_pair(i,make_pair(j,k))));
			}
		}
	}
}

void PTracker::interruptCallback(int)
{
	ERR(endl << "*********************************************************************" << endl);
	ERR("Caught Ctrl+C. Exiting..." << endl);
	ERR("*********************************************************************" << endl);
	
	exit(0);
}

void PTracker::exec(const ObjectSensorReading& visualReading, ofstream* results)
{
	static UdpSocket senderSocket;
	
	vector<ObjectSensorReading> observations;
	string dataToSend;
	int ret;
	
	currentTimestamp.setToNow();
	
	++counterResult;
	
#ifdef DEBUG_MODE
	INFO("[PTracker (" << agentId << ")] - ***********************************" << endl);
	INFO("[PTracker (" << agentId << ")] - \tNEW ITERATION (" << counterResult << ")" << endl);
	INFO("[PTracker (" << agentId << ")] - ***********************************" << endl);
#else
	INFO(".");
#endif
	
	updateTargetVector(visualReading);
	
	agentPose = visualReading.getObservationsAgentPose();
	
	objectSensorReading.setObservationsAgentPose(agentPose);
	objectSensorReading.setObservations(targetVector,currentTargetIndex,lastCurrentTargetIndex,LAST_N_TARGET_PERCEPTIONS,worldX,worldY);
	
	observations.push_back(objectSensorReading);
	
	processor.processReading(agentPose,initialTimestamp,currentTimestamp,observations);
	
	const SingleAgentEstimations& estimationsWithModel = objectParticleFilter.getEstimationsWithModel();
	
	updateTargetPosition(estimationsWithModel);
	bestParticles = updateBestParticles(estimationsWithModel);
	
	if (estimatedTargetModels.size() > 0)
	{
		AgentPacket agentPacket;
		
		agentPacket.dataPacket.ip = agentAddress;
		agentPacket.dataPacket.port = agentPort;
		agentPacket.dataPacket.agentPose = agentPose;
		agentPacket.dataPacket.estimatedTargetModels = calibrate(estimatedTargetModels);
		agentPacket.dataPacket.particlesTimestamp = currentTimestamp.getMsFromMidnight();
		
		dataToSend = "Agent ";
		dataToSend += agentPacket.toString();
		
		if ((Timestamp() - lastTimeInformationSent).getMs() > (1000.0 / messageFrequency))
		{
			sendEstimationsToAgents(dataToSend);
			
			lastTimeInformationSent.setToNow();
		}
		
		ObjectSensorReadingMultiAgent objectSensorReadingMultiAgent;
		
		objectSensorReadingMultiAgent.setAgent(agentAddress,agentPort);
		objectSensorReadingMultiAgent.setSensor(objectParticleFilter.getSensor());
		objectSensorReadingMultiAgent.setEstimationsWithModels(calibrate(estimatedTargetModels));
		objectSensorReadingMultiAgent.setEstimationsTimestamp(currentTimestamp.getMsFromMidnight());
		
		mutex.lock();
		
		observationsMultiAgent.push_back(objectSensorReadingMultiAgent);
		
		mutex.unlock();
	}
	
	initialTimestamp = currentTimestamp;
	++iterationCounter;
	
	if (iterationCounter == 1)
	{
		iterationCounter = 0;
		initialTimestampMas = currentTimestamp;
		
		mutex.lock();
		
		multiAgentProcessor.processReading(observationsMultiAgent);
		estimatedTargetModelsMultiAgent = objectParticleFilterMultiAgent.getEstimationsWithModel();
		
		observationsMultiAgent.clear();
		
		mutex.unlock();
		
		if (estimatedTargetModelsMultiAgent.size() > 0)
		{
			dataToSend = prepareDataForViewer();
			
			ret = senderSocket.send(dataToSend,InetAddress(pViewerAddress,pViewerPort));
			
			if (ret == -1)
			{
				ERR("Error when sending message to PViewer." << endl);
			}
		}
		
		if (rosBridgeEnabled)
		{
			stringstream s;
			
			for (MultiAgentEstimations::const_iterator it = estimatedTargetModelsMultiAgent.begin(); it != estimatedTargetModelsMultiAgent.end(); ++it)
			{
				s << it->first << " " << it->second.first.first.observation.x << " " << it->second.first.first.observation.y << " " << it->second.first.second.x << " " << it->second.first.second.y << " "
				  << it->second.first.first.model.width << " " << it->second.first.first.model.height << " " << it->second.first.first.model.velocity.x << " " << it->second.first.first.model.velocity.y << " "
				  << it->second.first.first.model.averagedVelocity.x << " " << it->second.first.first.model.averagedVelocity.y << " ; ";
			}
			
			ret = senderSocket.send(s.str().substr(0,s.str().size() - 3),InetAddress(rosBridgeAddress,rosBridgePort));
			
			if (ret == -1)
			{
				ERR("Error when sending message to the ros node bridge." << endl);
			}
		}
	}
	
	if ((results != 0) && (results->is_open()))
	{
		*results << "   <frame number=\"" << counterResult << "\">" << endl;
		*results << "      <objectlist>" << endl;
		
		for (MultiAgentEstimations::const_iterator it = estimatedTargetModelsMultiAgent.begin(); it != estimatedTargetModelsMultiAgent.end(); ++it)
		{
			*results << "         <object id=\"" << it->first << "\">" << endl;
			*results << "            <box h=\"" << it->second.first.first.model.height << "\" w=\"" << it->second.first.first.model.width << "\""
					 << " xc=\"" << it->second.first.first.observation.x << "\" yc=\"" << it->second.first.first.observation.y << "\""
					 << " hxc=\"" << it->second.first.first.head.x << "\" hyc=\"" << it->second.first.first.head.y << "\""
					 << " b=\"" << it->second.first.first.model.barycenter << "\"/>" << endl;
			*results << "         </object>" << endl;
		}
		
		*results << "      </objectlist>" << endl;
		*results << "   </frame>" << endl;
	}
	
	lastCurrentTargetIndex = currentTargetIndex;
	
#ifdef DEBUG_MODE
	for (MultiAgentEstimations::const_iterator it = estimatedTargetModelsMultiAgent.begin(); it != estimatedTargetModelsMultiAgent.end(); ++it)
	{
		WARN("[PTracker (" << agentId << ")] - target estimation global frame -> (" << it->first << ",[" << it->second.first.first.observation.x << ","
																										 << it->second.first.first.observation.y << "],"
																										 << "velocity = [" << it->second.first.first.model.velocity.x << ","
																										 << it->second.first.first.model.velocity.y << "],"
																										 << "averaged velocity = [" << it->second.first.first.model.averagedVelocity.x << ","
																										 << it->second.first.first.model.averagedVelocity.y << "])" << endl);
	}
	
	INFO("Time: " << (Timestamp() - currentTimestamp).getMs() << endl);
#endif
}

void PTracker::exec(const string& observationFile)
{
	ofstream results;
	string resultFile;
	float sleepTime;
	
	const vector<ObjectSensorReading>& visualReadings = Utils::readObservationFile(observationFile);
	
	/// Avoiding compilation warning.
	if (system("mkdir -p ../results/tracker"));
	
	resultFile = string("../results/tracker/PTracker-") + observationFile.substr(observationFile.rfind("/") + 1);
	
	results.open(resultFile.c_str());
	
	if (results.is_open())
	{
		results << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
		results << "<dataset>" << endl;
	}
	
	if (frameRate > 0) sleepTime = (1000.0 / frameRate) * 1000.0;
	else sleepTime = 30e3;
	
	for (vector<ObjectSensorReading>::const_iterator it = visualReadings.begin(); it != visualReadings.end(); ++it)
	{
		exec(*it);
		
		if (results.is_open())
		{
			results << "   <frame number=\"" << counterResult << "\">" << endl;
			results << "      <objectlist>" << endl;
			
			for (MultiAgentEstimations::const_iterator it2 = estimatedTargetModelsMultiAgent.begin(); it2 != estimatedTargetModelsMultiAgent.end(); ++it2)
			{
				results << "         <object id=\"" << it2->first << "\">" << endl;
				results << "            <box h=\"" << it2->second.first.first.model.height << "\" w=\"" << it2->second.first.first.model.width << "\""
						<< " xc=\"" << it2->second.first.first.observation.x << "\" yc=\"" << it2->second.first.first.observation.y << "\""
						<< " hxc=\"" << it2->second.first.first.head.x << "\" hyc=\"" << it2->second.first.first.head.y << "\""
						<< " b=\"" << it2->second.first.first.model.barycenter << "\"/>" << endl;
				results << "         </object>" << endl;
			}
			
			results << "      </objectlist>" << endl;
			results << "   </frame>" << endl;
		}
		
		usleep(sleepTime);
	}
	
	if (results.is_open())
	{
		results << "</dataset>" << endl;
		results.close();
		
		INFO(endl << "Results has been saved in ");
		WARN(resultFile);
		INFO("." << endl);
	}
	else ERR(endl << "An error occured during the writing process. Results are not available..." << endl);
}

string PTracker::prepareDataForViewer() const
{
	stringstream streamDataToSend;
	int i;
	
	streamDataToSend << buildHeader();
	
	//streamDataToSend << "1 " << Utils::Point2ofOnMap << " " << Utils::roundN(agentPose.x,2) << " " << Utils::roundN(agentPose.y,2) << " " << Utils::roundN(agentPose.theta,2);
	
	for (MultiAgentEstimations::const_iterator it = estimatedTargetModelsMultiAgent.begin(); it != estimatedTargetModelsMultiAgent.end(); ++it)
	{
		PTracking::PointWithVelocity endPoint = Utils::estimatedPosition(it->second.first.first.observation,it->second.first.first.model.averagedVelocity,1);
		
		streamDataToSend << " 1 " << Utils::Point2fWithVelocityOnMap << " " << Utils::roundN(it->second.first.first.observation.x,2) << " " << Utils::roundN(it->second.first.first.observation.y,2)
						 << " " << Utils::roundN(endPoint.pose.x,2) << " " << Utils::roundN(endPoint.pose.y,2);
	}
	
	if (abs(currentTargetIndex - lastCurrentTargetIndex) > 0)
	{
		streamDataToSend << " " << ((lastCurrentTargetIndex > currentTargetIndex) ? ((LAST_N_TARGET_PERCEPTIONS - lastCurrentTargetIndex) + currentTargetIndex)
																				  : (currentTargetIndex - lastCurrentTargetIndex));
	}
	
	i = lastCurrentTargetIndex;
	
	while (i != currentTargetIndex)
	{
		streamDataToSend << " " << Utils::Point2fOnMap << " " << Utils::roundN(targetVector[i].observation.x,2) << " " << Utils::roundN(targetVector[i].observation.y,2);
		
		++i;
		
		if (i == LAST_N_TARGET_PERCEPTIONS) i = 0;
	}
	
	const map<int,pair<ObjectSensorReading::Observation,Point2of> >& observationsMapping = objectParticleFilter.getObservationsMapping();
	
	for (map<int,pair<ObjectSensorReading::Observation,Point2of> >::const_iterator it = observationsMapping.begin(); it != observationsMapping.end(); ++it)
	{
		streamDataToSend << " 1 " << Utils::Line2dOnMap << " " << Utils::roundN(it->second.first.observation.x,2) << " " << Utils::roundN(it->second.first.observation.y,2)
						 << " " << Utils::roundN(it->second.second.x,2) << " " << Utils::roundN(it->second.second.y,2);
	}
	
	return streamDataToSend.str();
}

void PTracker::readCalibrationData(const string& calibrationDirectory)
{
	ConfigFile fCfg;
	stringstream s;
	string key, scenario, section;
	
	if (!fCfg.read(calibrationDirectory + string("/homography.cfg")))
	{
		ERR("Error reading file '" << (calibrationDirectory + string("/homography.cfg")) << "'. Exiting..." << endl);
		
		exit(-1);
	}
	
	try
	{
		section = "parameters";
		key = "resolution";
		
		resolution = fCfg.value(section,key);
	}
	catch (...)
	{
		ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
		
		exit(-1);
	}
	
	scenario = calibrationDirectory;
	
	/// The last character is a "/", so we have to remove it before getting the name of the scenario.
	if (scenario.rfind("/") == (scenario.size() - 1)) scenario = scenario.substr(0,scenario.rfind("/"));
	
	s << scenario.substr(scenario.rfind("/") + 1) << "_" << agentId;
	
	section = s.str();
	
	try
	{
		key = "H00";
		H.at<float>(0,0) = fCfg.value(section,key);
		
		key = "H01";
		H.at<float>(0,1) = fCfg.value(section,key);
		
		key = "H02";
		H.at<float>(0,2) = fCfg.value(section,key);
		
		key = "H10";
		H.at<float>(1,0) = fCfg.value(section,key);
		
		key = "H11";
		H.at<float>(1,1) = fCfg.value(section,key);
		
		key = "H12";
		H.at<float>(1,2) = fCfg.value(section,key);
		
		key = "H20";
		H.at<float>(2,0) = fCfg.value(section,key);
		
		key = "H21";
		H.at<float>(2,1) = fCfg.value(section,key);
		
		key = "H22";
		H.at<float>(2,2) = fCfg.value(section,key);
	}
	catch (...)
	{
		ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
		
		exit(-1);
	}
	
	section = s.str() + string("-inverse");
	
	try
	{
		key = "H00";
		Hinv.at<float>(0,0) = fCfg.value(section,key);
		
		key = "H01";
		Hinv.at<float>(0,1) = fCfg.value(section,key);
		
		key = "H02";
		Hinv.at<float>(0,2) = fCfg.value(section,key);
		
		key = "H10";
		Hinv.at<float>(1,0) = fCfg.value(section,key);
		
		key = "H11";
		Hinv.at<float>(1,1) = fCfg.value(section,key);
		
		key = "H12";
		Hinv.at<float>(1,2) = fCfg.value(section,key);
		
		key = "H20";
		Hinv.at<float>(2,0) = fCfg.value(section,key);
		
		key = "H21";
		Hinv.at<float>(2,1) = fCfg.value(section,key);
		
		key = "H22";
		Hinv.at<float>(2,2) = fCfg.value(section,key);
	}
	catch (...)
	{
		ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
		
		exit(-1);
	}
}

bool PTracker::reset()
{
	return (objectParticleFilter.checkFilterForReinitialization(true) && objectParticleFilterMultiAgent.reset());
}

void PTracker::sendEstimationsToAgents(const string& dataToSend) const
{
	UdpSocket senderSocket;
	int ret;
	
	for (vector<pair<string,int> >::const_iterator it = receivers.begin(); it != receivers.end(); it++)
	{
		ret = senderSocket.send(dataToSend,InetAddress(it->first,it->second));
		
		if (ret == -1)
		{
			ERR("Error when sending message to: '" << it->second << "'." << endl);
		}
	}
}

vector<PoseParticleVector> PTracker::updateBestParticles(const SingleAgentEstimations& estimationsWithModel)
{
	bestParticles.clear();
	
	for (SingleAgentEstimations::const_iterator it = estimationsWithModel.begin(); it != estimationsWithModel.end(); ++it)
	{
		bestParticles.push_back(Utils::samplingParticles(it->second.first.observation,it->second.first.sigma,bestParticlesNumber));
	}
	
	return bestParticles;
}

void PTracker::updateTargetPosition(const SingleAgentEstimations& estimationsWithModel)
{
	estimatedTargetModels.clear();
	
	for (SingleAgentEstimations::const_iterator it = estimationsWithModel.begin(); it != estimationsWithModel.end(); it++)
	{
		estimatedTargetModels.insert(*it);
		
#ifdef DEBUG_MODE
		ERR("[PTracker (" << agentId << ")] - target estimation local frame -> (" << it->first << ",[" << it->second.first.observation.x << ","
																									   << it->second.first.observation.y << "])" << endl);
#endif
	}
}

void PTracker::updateTargetVector(const ObjectSensorReading& visualReading)
{
	const vector<ObjectSensorReading::Observation>& observations = visualReading.getObservations();
	
	for (vector<ObjectSensorReading::Observation>::const_iterator it = observations.begin(); it != observations.end(); ++it)
	{
#ifdef DEBUG_MODE
		DEBUG("[PTracker (" << agentId << ")] - observation -> (" << it->observation.x << "," << it->observation.y << ")" << endl);
#endif
		
		targetVector[currentTargetIndex] = *it;
		
		currentTargetIndex++;
		lastTargetIndex++;
		
		if (currentTargetIndex == LAST_N_TARGET_PERCEPTIONS) currentTargetIndex = 0;
		
		if (lastTargetIndex == LAST_N_TARGET_PERCEPTIONS) lastTargetIndex = 0;
		
		if (maxTargetIndex < LAST_N_TARGET_PERCEPTIONS) maxTargetIndex++;
	}
}

SingleAgentEstimations PTracker::uncalibrate(const SingleAgentEstimations& estimations) const
{
	if (!calibrateData) return estimations;
	
	SingleAgentEstimations uncalibratedEstimations;
	
	for (SingleAgentEstimations::const_iterator it = estimations.begin(); it != estimations.end(); ++it)
	{
		ObjectSensorReading::Observation uncalibratedEstimation;
		float imageX, imageY, imageHeadX, imageHeadY, worldX, worldY, worldHeadX, worldHeadY;
		
		worldX = it->second.first.observation.x / resolution;
		worldY = it->second.first.observation.y / resolution;
		
		worldHeadX = it->second.first.head.x / resolution;
		worldHeadY = it->second.first.head.y / resolution;
		
		imageX = (Hinv.at<float>(0,0) * worldX + Hinv.at<float>(0,1) * worldY + Hinv.at<float>(0,2)) / (Hinv.at<float>(2,0) * worldX + Hinv.at<float>(2,1) * worldY + Hinv.at<float>(2,2));
		imageY = (Hinv.at<float>(1,0) * worldX + Hinv.at<float>(1,1) * worldY + Hinv.at<float>(1,2)) / (Hinv.at<float>(2,0) * worldX + Hinv.at<float>(2,1) * worldY + Hinv.at<float>(2,2));
		
		imageHeadX = (Hinv.at<float>(0,0) * worldHeadX + Hinv.at<float>(0,1) * worldHeadY + Hinv.at<float>(0,2)) / (Hinv.at<float>(2,0) * worldHeadX + Hinv.at<float>(2,1) * worldHeadY + Hinv.at<float>(2,2));
		imageHeadY = (Hinv.at<float>(1,0) * worldHeadX + Hinv.at<float>(1,1) * worldHeadY + Hinv.at<float>(1,2)) / (Hinv.at<float>(2,0) * worldHeadX + Hinv.at<float>(2,1) * worldHeadY + Hinv.at<float>(2,2));
		
		/// Copying all data that do not change because of the calibration.
		uncalibratedEstimation = it->second.first;
		
		uncalibratedEstimation.observation.x = imageX;
		uncalibratedEstimation.observation.y = imageY;
		uncalibratedEstimation.head.x = imageHeadX;
		uncalibratedEstimation.head.y = imageHeadY;
		uncalibratedEstimation.model.averagedVelocity.x /= resolution;
		uncalibratedEstimation.model.averagedVelocity.y /= resolution;
		uncalibratedEstimation.model.velocity.x /= resolution;
		uncalibratedEstimation.model.velocity.y /= resolution;
		
		uncalibratedEstimations.insert(make_pair(it->first,make_pair(uncalibratedEstimation,it->second.second)));
	}
	
	return uncalibratedEstimations;
}

MultiAgentEstimations PTracker::uncalibrate(const MultiAgentEstimations& estimations) const
{
	if (!calibrateData) return estimations;
	
	MultiAgentEstimations uncalibratedEstimations;
	
	for (MultiAgentEstimations::const_iterator it = estimations.begin(); it != estimations.end(); ++it)
	{
		ObjectSensorReading::Observation uncalibratedEstimation;
		float imageX, imageY, imageHeadX, imageHeadY, worldX, worldY, worldHeadX, worldHeadY;
		
		worldX = it->second.first.first.observation.x / resolution;
		worldY = it->second.first.first.observation.y / resolution;
		
		worldHeadX = it->second.first.first.head.x / resolution;
		worldHeadY = it->second.first.first.head.y / resolution;
		
		imageX = (Hinv.at<float>(0,0) * worldX + Hinv.at<float>(0,1) * worldY + Hinv.at<float>(0,2)) / (Hinv.at<float>(2,0) * worldX + Hinv.at<float>(2,1) * worldY + Hinv.at<float>(2,2));
		imageY = (Hinv.at<float>(1,0) * worldX + Hinv.at<float>(1,1) * worldY + Hinv.at<float>(1,2)) / (Hinv.at<float>(2,0) * worldX + Hinv.at<float>(2,1) * worldY + Hinv.at<float>(2,2));
		
		imageHeadX = (Hinv.at<float>(0,0) * worldHeadX + Hinv.at<float>(0,1) * worldHeadY + Hinv.at<float>(0,2)) / (Hinv.at<float>(2,0) * worldHeadX + Hinv.at<float>(2,1) * worldHeadY + Hinv.at<float>(2,2));
		imageHeadY = (Hinv.at<float>(1,0) * worldHeadX + Hinv.at<float>(1,1) * worldHeadY + Hinv.at<float>(1,2)) / (Hinv.at<float>(2,0) * worldHeadX + Hinv.at<float>(2,1) * worldHeadY + Hinv.at<float>(2,2));
		
		/// Copying all data that do not change because of the calibration.
		uncalibratedEstimation = it->second.first.first;
		
		uncalibratedEstimation.observation.x = imageX;
		uncalibratedEstimation.observation.y = imageY;
		uncalibratedEstimation.head.x = imageHeadX;
		uncalibratedEstimation.head.y = imageHeadY;
		uncalibratedEstimation.model.averagedVelocity.x /= resolution;
		uncalibratedEstimation.model.averagedVelocity.y /= resolution;
		uncalibratedEstimation.model.velocity.x /= resolution;
		uncalibratedEstimation.model.velocity.y /= resolution;
		
		uncalibratedEstimations.insert(make_pair(it->first,make_pair(make_pair(uncalibratedEstimation,it->second.first.second),it->second.second)));
	}
	
	return uncalibratedEstimations;
}

void PTracker::updateTimeToWaitBeforeDeleting(float frameRate)
{
	objectParticleFilter.updateTimeToWaitBeforeDeleting(frameRate);
	objectParticleFilterMultiAgent.updateTimeToWaitBeforeDeleting(frameRate);
}

void PTracker::updateTimeToWaitBeforePromoting(float frameRate)
{
	objectParticleFilter.updateTimeToWaitBeforePromoting(frameRate);
}

void PTracker::waitAgentMessages()
{
	ObjectSensorReadingMultiAgent objectSensorReadingMultiAgent;
	UdpSocket receiverSocket;
	InetAddress sender;
	string dataReceived;
	int ret;
	bool binding;
	
	binding = receiverSocket.bind(agentPort);
	
	if (!binding)
	{
		ERR("Error during the binding operation. Data fusion among agents is not possible. If you are running multiple instances remember to set a unique agentId for each agent. Exiting..." << endl);
		
		exit(-1);
	}
	
	objectSensorReadingMultiAgent.setSensor(objectSensorReading.getSensor());
	
	while (true)
	{
		ret = receiverSocket.recv(dataReceived,sender);
		
		if (ret == -1)
		{
			ERR("Error in receiving message from: '" << sender.toString() << "'." << endl);
			
			continue;
		}
		
		AgentPacket ap;
		
		ap.setData(dataReceived.substr(dataReceived.find(" ") + 1));
		
		objectSensorReadingMultiAgent.setAgent(ap.dataPacket.ip,ap.dataPacket.port);
		objectSensorReadingMultiAgent.setEstimationsWithModels(ap.dataPacket.estimatedTargetModels);
		objectSensorReadingMultiAgent.setEstimationsTimestamp(ap.dataPacket.particlesTimestamp);
		
		mutex.lock();
		
		observationsMultiAgent.push_back(objectSensorReadingMultiAgent);
		
		mutex.unlock();
	}
}
