/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Core/Processors/MultiAgentProcessor.h>
#include <Core/Processors/Processor.h>
#include <Utils/AgentPacket.h>
#include <Utils/Typedef.h>
#include <opencv2/core/core.hpp>
#include <mutex>

/**
 * @class PTracker
 * 
 * @brief Class that implements a distributed tracker using a distributed particle filtering method.
 * 
 * PTracker allows to perform the distributed tracking in two ways:
 *	- by giving as input a file containing all the observations
 *	- by calling an exec function that performs the distributed tracking iteration by iteration
 */
class PTracker
{
	private:
		/**
		 * @brief maximum size of the circular buffer containing the observations coming from the sensors.
		 */
		static const int LAST_N_TARGET_PERCEPTIONS = 100;
		
		/**
		 * @brief map representing the estimations having both an identity and a model of the estimations performed by the team of agents.
		 */
		PTracking::MultiAgentEstimations estimatedTargetModelsMultiAgent;
		
		/**
		 * @brief map of estimations having both an identity and a model performed by the agent.
		 */
		PTracking::SingleAgentEstimations estimatedTargetModels;
		
		/**
		 * @brief vector of the best particles.
		 */
		std::vector<PoseParticleVector> bestParticles;
		
		/**
		 * @brief vector containing all the addresses of the agents that have to receive the information.
		 */
		std::vector<std::pair<std::string,int> > receivers;
		
		/**
		 * @brief vector containing all the estimations performed the team of agents.
		 */
		std::vector<PTracking::ObjectSensorReadingMultiAgent> observationsMultiAgent;
		
		/**
		 * @brief map containing the mapping between identity and color.
		 */
		std::map<int,std::pair<int,std::pair<int,int> > > colorMap;
		
		/**
		 * @brief calibration matrix.
		 */
		cv::Mat H;
		
		/**
		 * @brief uncalibration matrix.
		 */
		cv::Mat Hinv;
		
		/**
		 * @brief single agent processor.
		 */
		PTracking::Processor processor;
		
		/**
		 * @brief multi agent processor.
		 */
		PTracking::MultiAgentProcessor multiAgentProcessor;
		
		/**
		 * @brief particle filter for the local estimation layer.
		 */
		PTracking::ObjectParticleFilter objectParticleFilter;
		
		/**
		 * @brief particle filter for the global estimation layer.
		 */
		PTracking::ObjectParticleFilterMultiAgent objectParticleFilterMultiAgent;
		
		/**
		 * @brief circular buffer containing the observations coming from the sensors.
		 */
		PTracking::ObjectSensorReading::Observation targetVector[LAST_N_TARGET_PERCEPTIONS];
		
		/**
		 * @brief model of the sensor reading.
		 */
		PTracking::ObjectSensorReading objectSensorReading;
		
		/**
		 * @brief position of the agent.
		 */
		PTracking::Point2of agentPose;
		
		/**
		 * @brief maximum admissible range for the x coordinate.
		 */
		PTracking::Point2f worldX;
		
		/**
		 * @brief maximum admissible range for the y coordinate.
		 */
		PTracking::Point2f worldY;
		
		/**
		 * @brief timestamp of the current iteration.
		 */
		PTracking::Timestamp currentTimestamp;
		
		/**
		 * @brief timestamp representing the starting time of the iteration.
		 */
		PTracking::Timestamp initialTimestamp;
		
		/**
		 * @brief timestamp representing the starting time of the multi agent iteration.
		 */
		PTracking::Timestamp initialTimestampMas;
		
		/**
		 * @brief timestamp representing the time when the last information to the team of agents have been sent.
		 */
		PTracking::Timestamp lastTimeInformationSent;
		
		/**
		 * @brief semaphore to handle the mutual exclusion between the single agent and the multi agent phase.
		 */
		std::mutex mutex;
		
		/**
		 * @brief address of the agent.
		 */
		std::string agentAddress;
		
		/**
		 * @brief address of PViewer.
		 */
		std::string pViewerAddress;
		
		/**
		 * @brief address of the ros node bridge.
		 */
		std::string rosBridgeAddress;
		
		/**
		 * @brief number of frames per second.
		 */
		float frameRate;
		
		/**
		 * @brief maximum range of the sensor.
		 */
		float maxReading;
		
		/**
		 * @brief frequency by which the information are sent to the team of agents.
		 */
		float messageFrequency;
		
		/**
		 * @brief pixel/meter ratio for calibrating/uncalibrating data.
		 */
		float resolution;
		
		/**
		 * @brief id of the agent.
		 */
		int agentId;
		
		/**
		 * @brief port of the agent.
		 */
		int agentPort;
		
		/**
		 * @brief number of best particles.
		 */
		int bestParticlesNumber;
		
		/**
		 * @brief counter used when writing the file of the results.
		 */
		int counterResult;
		
		/**
		 * @brief current index of the circular buffer of the observations.
		 */
		int currentTargetIndex;
		
		/**
		 * @brief counter of the iterations.
		 */
		int iterationCounter;
		
		/**
		 * @brief last current index of the circular buffer of the observations.
		 */
		int lastCurrentTargetIndex;
		
		/**
		 * @brief last target index of the circular buffer of the observations.
		 */
		int lastTargetIndex;
		
		/**
		 * @brief maximum index of the circular buffer of the observations.
		 */
		int maxTargetIndex;
		
		/**
		 * @brief port of PViewer.
		 */
		int pViewerPort;
		
		/**
		 * @brief port of the ros node bridge.
		 */
		int rosBridgePort;
		
		/**
		 * @brief if enabled, the tracking will provide in output calibrated data.
		 */
		bool calibrateData;
		
		/**
		 * @brief enabling/disabling communication with the ros node bridge.
		 */
		bool rosBridgeEnabled;
		
		/**
		 * @brief Function that invokes a thread-function that waits messages coming from other agents.
		 * 
		 * @param pTracker pointer to the invocation object.
		 * 
		 * @return 0 if succeeded, -1 otherwise.
		 */
		static void* waitAgentMessagesThread(PTracker* pTracker) { pTracker->waitAgentMessages(); return 0; }
		
		/**
		 * @brief Function that allows a clean exit catching the SIGINT signal.
		 */
		static void interruptCallback(int);
		
		/**
		 * @brief Function that constructs the header of the message to send to PViewer.
		 * 
		 * @return the header of the message for PViewer.
		 */
		std::string buildHeader() const;
		
		/**
		 * @brief Function that reads a config file in order to initialize several configuration parameters.
		 * 
		 * @param filename file to be read.
		 * @param calibrationDirectory directory where the calibration data have to be read.
		 */
		void configure(const std::string& filename, const std::string& calibrationDirectory);
		
		/**
		 * @brief Function that initializes several configuration parameters.
		 * 
		 * @param frameRate number of frames per second.
		 * @param filename file to be read.
		 * @param calibrationDirectory directory where the calibration data have to be read.
		 */
		void init(float frameRate, std::string filename = "", const std::string& calibrationDirectory = "");
		
		/**
		 * @brief Function that constructs the message for PViewer containing all the necessary information.
		 * 
		 * @return the message for PViewer.
		 */
		std::string prepareDataForViewer() const;
		
		/**
		 * @brief Function that reads the calibration data.
		 * 
		 * @param calibrationDirectory directory where the calibration data have to be read.
		 */
		void readCalibrationData(const std::string& calibrationDirectory);
		
		/**
		 * @brief Function that sends the estimations to all the agents.
		 * 
		 * @param dataToSend reference to the estimations to send (string format).
		 */
		void sendEstimationsToAgents(const std::string& dataToSend) const;
		
		/**
		 * @brief Function that returns the best particles representing the current estimations performed by the single agent.
		 * 
		 * @param estimationsWithModel reference of the current estimations performed by the single agent.
		 * 
		 * @return the vector containing the best particles of the current estimations.
		 */
		std::vector<PoseParticleVector> updateBestParticles(const PTracking::SingleAgentEstimations& estimationsWithModel);
		
		/**
		 * @brief Function that updates the estimations performed by the single agent.
		 * 
		 * @param estimationsWithModel reference of the current estimations performed by the single agent.
		 */
		void updateTargetPosition(const PTracking::SingleAgentEstimations& estimationsWithModel);
		
		/**
		 * @brief Function that updates the observations coming from the sensors.
		 * 
		 * @param visualReading reference to the current observations coming from the sensors.
		 */
		void updateTargetVector(const PTracking::ObjectSensorReading& visualReading);
		
		/**
		 * @brief Function that collects messages coming from the other agents.
		 */
		void waitAgentMessages();
		
	public:
		/**
		 * @brief Constructor that takes the frameRate as initialization value.
		 * 
		 * @param frameRate number of frames per second.
		 */
		PTracker(float frameRate);
		
		/**
		 * @brief Constructor that takes the agent id as initialization value.
		 * 
		 * It initializes the agent id with the one given as input.
		 * 
		 * @param agentId id of the agent.
		 * @param filename file to be read.
		 * @param calibrationDirectory directory where the calibration data have to be read.
		 * @param frameRate number of frames per second.
		 */
		PTracker(int agentId, const std::string& filename = "", const std::string& calibrationDirectory = "", float frameRate = 30);
		
		/**
		 * @brief Destructor.
		 */
		~PTracker();
		
		/**
		 * @brief Function that returns \b true if data are calibrated, \b false otherwise.
		 * 
		 * @return \b true if data are calibrated, \b false otherwise.
		 */
		inline bool areDataCalibrated() const { return calibrateData; }
		
		/**
		 * @brief Function that calibrates the estimations given as input.
		 * 
		 * @param estimations reference to the estimations to be calibrated.
		 * 
		 * @return the calibrated estimations, if requested.
		 */
		PTracking::SingleAgentEstimations calibrate(const PTracking::SingleAgentEstimations& estimations) const;
		
		/**
		 * @brief Function that gets the observations and perform the distributed tracking iteration by iteration. It can be stopped by pressing Ctrl+C.
		 * 
		 * @param visualReading reference to the current observations coming from the sensors.
		 * @param results file in which the tracking results will be saved (if NULL no results will be saved).
		 */
		void exec(const PTracking::ObjectSensorReading& visualReading, std::ofstream* results = 0);
		
		/**
		 * @brief Function that reads the observation file and perform the distributed tracking. The results are written in a file. It can be stopped by pressing Ctrl+C.
		 * 
		 * @param observationFile reference to the file containing all the observations.
		 */
		void exec(const std::string& observationFile);
		
		/**
		 * @brief Function that returns the estimations performed by the single agent and by the team of agents.
		 * 
		 * @return a pair containing both the estimations performed by the single agent and by the team of agents.
		 */
		inline std::pair<PTracking::MultiAgentEstimations,PTracking::SingleAgentEstimations> getAgentEstimations() const { return std::make_pair(estimatedTargetModelsMultiAgent,calibrate(estimatedTargetModels)); }
		
		/**
		 * @brief Function that returns the group estimations performed by the single agent.
		 * 
		 * @return a vector containing the group estimations performed by the single agent.
		 */
		inline std::vector<std::pair<std::string,std::pair<PTracking::ObjectSensorReading::Observation,PTracking::Point2f> > > getAgentGroupEstimations() const { return objectParticleFilter.getGroupEstimationsWithModel(); }
		
		/**
		 * @brief Function that returns the pixel/meter ratio for calibrating/uncalibrating data.
		 * 
		 * @return the pixel/meter ratio for calibrating/uncalibrating data.
		 */
		inline float getResolution() const { return resolution; }
		
		/**
		 * @brief Function that resets the particle filter used in the local frame.
		 * 
		 * @return \b true if the reset has been successfull, \b false otherwise.
		 */
		bool reset();
		
		/**
		 * @brief Function that uncalibrates the estimations given as input.
		 * 
		 * @param estimations reference to the estimations to be uncalibrated.
		 * 
		 * @return the uncalibrated estimations, if requested.
		 */
		PTracking::SingleAgentEstimations uncalibrate(const PTracking::SingleAgentEstimations& estimations) const;
		
		/**
		 * @brief Function that uncalibrates the estimations given as input.
		 * 
		 * @param estimations reference to the estimations to be uncalibrated.
		 * 
		 * @return the uncalibrated estimations, if requested.
		 */
		PTracking::MultiAgentEstimations uncalibrate(const PTracking::MultiAgentEstimations& estimations) const;
		
		/**
		 * @brief Function that updates the time to wait before deleting an estimation no longer associated to an observation coming from the agent's sensors, based on the current frame rate.
		 * 
		 * @param frameRate number of frames per second.
		 */
		void updateTimeToWaitBeforeDeleting(float frameRate);
		
		/**
		 * @brief Function that updates the time to wait before promoting an observation coming from the agent's sensors, based on the current frame rate.
		 * 
		 * @param frameRate number of frames per second.
		 */
		void updateTimeToWaitBeforePromoting(float frameRate);
};
