/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <opencv2/core/core.hpp>
#include <stdexcept>

namespace Drivers
{
	/**
	 * @class BasicDriver
	 * 
	 * @brief Abstract class that defines an interface for a driver of a sensor (e.g., Kinect or Xtion).
	 */
	class BasicDriver
	{
		protected:
			/**
			 * @brief RGB image of the sensor.
			 */
			cv::Mat frame;
			
		public:
			/**
			 * @brief Destructor.
			 */
			virtual ~BasicDriver() {;}
			
			/**
			 * @brief Function that reads the RGB image from the sensor.
			 * 
			 * @return the RGB image of the sensor.
			 */
			virtual const cv::Mat& getData() = 0;
			
			/**
			 * @brief Function that reads the depth image from the sensor.
			 * 
			 * @return the depth image of the sensor.
			 */
			virtual const cv::Mat& getDepthData() { throw std::runtime_error("BasicDriver::getDepthData. This function has to be overloaded by the driver, to get depth data."); }
	};
}
