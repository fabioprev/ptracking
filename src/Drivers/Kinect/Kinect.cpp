/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "Kinect.h"
#include <Manfield/Utils/DebugUtils.h>

using namespace std;
using namespace cv;

namespace Drivers
{
	Kinect::Kinect()
	{
		searchSerialDevices();
		
		try
		{
			device = &freenect.createDevice<MyFreenectDevice>((char*) serial.c_str());
		}
		catch (const runtime_error& e)
		{
			ERR(e.what() << ". Exiting..." << endl);
			
			exit(-1);
		}
		
		device->startVideo();
		device->startDepth();
	}
	
	Kinect::~Kinect()
	{
		device->stopDepth();
		device->stopVideo();
		
		freenect.deleteDevice((char*) serial.c_str());
		
		delete device;
	}
	
	const Mat& Kinect::getData()
	{
		frame = device->getData();
		
		return frame;
	}
	
	const Mat& Kinect::getDepthData()
	{
		depthFrame = device->getDepthData();
		
		return depthFrame;
	}
	
	void Kinect::searchSerialDevices()
	{
		vector<string> serialList;
		freenect_context* freenectContext;
		int i, index;
		
		freenect_init(&freenectContext,0);
		
		struct freenect_device_attributes* list;
		struct freenect_device_attributes* item;
		
		freenect_list_device_attributes(freenectContext,&list);
		
		if (list == 0)
		{
			ERR("No devices found. Exiting..." << endl << endl);
			
			exit(-1);
		}
		
		INFO("Choose the device: " << endl);
		
		for (item = list, i = 0; item != 0; item = item->next, i++)
		{
			WARN("\t "<< i + 1 << " - Found a camera with serial number: ");
			ERR(item->camera_serial << endl);
			
			serialList.push_back(string(item->camera_serial));
		}
		
		INFO(endl);
		
		bool isValid;
		
		isValid = false;
		
		do
		{
			INFO("Device: ");
			
			cin >> index;
			
			if ((index == 0) || (index > (int) serialList.size()))
			{
				INFO("\033[1A");
				INFO("\033[K");
				
				cin.clear();
				cin.ignore(INT_MAX,'\n');
			}
			else isValid = true;
		}
		while (!isValid);
		
		--index;
		
		freenect_free_device_attributes(list);
		
		serial = serialList[index];
	}
}
