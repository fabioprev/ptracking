/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "MyFreenectDevice.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv/cvaux.h>

using namespace std;
using namespace cv;

namespace Drivers
{
	MyFreenectDevice::MyFreenectDevice(freenect_context* context, const string& serial) : Freenect::FreenectDevice(context,(char*) serial.c_str()), depthFrame(Size(FREENECT_FRAME_WIDTH,FREENECT_FRAME_HEIGHT),CV_16UC1),
																						  frame(Size(FREENECT_FRAME_WIDTH,FREENECT_FRAME_HEIGHT),CV_8UC3,Scalar(0)) {;}
	
	void MyFreenectDevice::DepthCallback(void* depth, uint32_t timestamp)
	{
		Mutex::ScopedLock mutex(depthMutex);
		
		mutex.lock();
		
		uint16_t* depthImage = static_cast<uint16_t*>(depth);
		
		for (int i = 0, offset = 0; i < FREENECT_FRAME_HEIGHT; ++i)
		{
			for (int j = 0; j < FREENECT_FRAME_WIDTH; ++j)
			{
				memcpy(&depthFrame.data[offset],&depthImage[i * FREENECT_FRAME_WIDTH + j],sizeof(short));
				
				offset += sizeof(short);
			}
		}
	}
	
	const Mat& MyFreenectDevice::getData()
	{
		Mutex::ScopedLock mutex(rgbMutex);
		
		mutex.lock();
		
		return frame;
	}
	
	const Mat& MyFreenectDevice::getDepthData()
	{
		Mutex::ScopedLock mutex(depthMutex);
		
		mutex.lock();
		
		return depthFrame;
	}
	
	void MyFreenectDevice::VideoCallback(void* rgb, uint32_t timestamp)
	{
		static vector<uint8_t> bufferRGB(FREENECT_VIDEO_RGB);
		
		Mutex::ScopedLock mutex(rgbMutex);
		
		mutex.lock();
		
		uint8_t* rgbImage = static_cast<uint8_t*>(rgb);
		
		copy(rgbImage,rgbImage + getVideoBufferSize(),bufferRGB.begin());
		
		memcpy(frame.data,rgbImage,FREENECT_VIDEO_RGB_SIZE);
		cvtColor(frame,frame,CV_RGB2BGR);
	}
}
