/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "Xtion.h"
#include <Manfield/Utils/DebugUtils.h>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;
using namespace openni;

namespace Drivers
{
	Xtion::Xtion()
	{
		openni::Status rc;
		int mode;
		
		/// Trying to initialize OpenNI2.
		rc = OpenNI::initialize();
		
		if (rc != STATUS_OK)
		{
			ERR("Initialize failed: " << OpenNI::getExtendedError() << "." << endl);
			
			exit(rc);
		}
		
		/// Trying to open device.
		rc = device.open(ANY_DEVICE);
		
		if (rc != STATUS_OK)
		{
			ERR("Could not open device: " << OpenNI::getExtendedError() << endl);
			
			exit(rc);
		}
		
		const SensorInfo* sensorsInfoColor = device.getSensorInfo(SENSOR_COLOR);
		const Array<VideoMode>& modesColor = sensorsInfoColor->getSupportedVideoModes();
		
		const SensorInfo* sensorsInfoDepth = device.getSensorInfo(SENSOR_DEPTH);
		const Array<VideoMode>& modesDepth = sensorsInfoDepth->getSupportedVideoModes();
		
		/// Trying to create color matrix.
		rc = color.create(device,SENSOR_COLOR);
		
		if (rc != STATUS_OK)
		{
			ERR("Could not create color stream: " << OpenNI::getExtendedError() << "." << endl);
			
			exit(rc);
		}
		
		if (device.getSensorInfo(SENSOR_DEPTH) != 0)
		{
			/// Trying to create depth matrix.
			rc = depth.create(device,SENSOR_DEPTH);
			
			if (rc != STATUS_OK)
			{
				ERR("Could not create depth stream: " << OpenNI::getExtendedError() << "." << endl);
				
				exit(rc);
			}
		}
		
		color.setMirroringEnabled(false);
		
		mode = -1;
		
		for (int i = 0; i < modesColor.getSize(); ++i)
		{
			if ((modesColor[i].getResolutionX() == FRAME_WIDTH) && (modesColor[i].getResolutionY() == FRAME_HEIGHT))
			{
				mode = i;
				
				break;
			}
		}
		
		if (mode == -1)
		{
			ERR("Could not set color stream resolution: ");
			INFO(FRAME_WIDTH << "x" << FRAME_HEIGHT << "." << endl);
			
			exit(-1);
		}
		
		/// Setting 640x480 30Hz video resolution mode.
		rc = color.setVideoMode(modesColor[mode]);
		
		if (rc != STATUS_OK)
		{
			ERR("Failed to set color resolution." << endl);
			
			exit(rc);
		}
		
		/// Trying to start color acquisition.
		rc = color.start();
		
		if (rc != STATUS_OK)
		{
			ERR("Could not start color stream: " << OpenNI::getExtendedError() << "." << endl);
			
			exit(rc);
		}
		
		depth.setMirroringEnabled(false);
		
		mode = -1;
		
		for (int i = 0; i < modesDepth.getSize(); ++i)
		{
			if ((modesDepth[i].getResolutionX() == FRAME_WIDTH) && (modesDepth[i].getResolutionY() == FRAME_HEIGHT))
			{
				mode = i;
				
				break;
			}
		}
		
		if (mode == -1)
		{
			ERR("Could not set depth stream resolution: ");
			INFO(FRAME_WIDTH << "x" << FRAME_HEIGHT << "." << endl);
			
			exit(-1);
		}
		
		/// Setting 640x480 30Hz video resolution mode.
		rc = depth.setVideoMode(modesDepth[mode]);
		
		if (rc != STATUS_OK)
		{
			ERR("Failed to set depth resolution." << endl);
			
			exit(rc);
		}
		
		/// Trying to start depth acquisition.
		rc = depth.start();
		
		if (rc != STATUS_OK)
		{
			ERR("Could not start depth stream: " << OpenNI::getExtendedError() << "." << endl);
			
			exit(rc);
		}
	}
	
	Xtion::~Xtion()
	{
		color.stop();
		depth.stop();
		
		device.close();
		
		OpenNI::shutdown();
	}
	
	const Mat& Xtion::getDepthData()
	{
		static VideoFrameRef frameDepth;
		static VideoStream* stream[] = { &depth };
		
		openni::Status rc;
		int changedStream;
		
		rc = OpenNI::waitForAnyStream(stream,1,&changedStream,2000);
		
		if (rc != STATUS_OK)
		{
			WARN("Wait failed (timeout 2000 ms). Error: " << OpenNI::getExtendedError() << "." << endl);
			
			exit(rc);
		}
		
		rc = depth.readFrame(&frameDepth);
		
		if (rc != STATUS_OK)
		{
			WARN("Reading depth matrix failed: " << OpenNI::getExtendedError() << "." << endl);
			
			exit(rc);
		}
		
		depthFrame = Mat(frameDepth.getHeight(),frameDepth.getWidth(),CV_16UC1,(void*) frameDepth.getData());
		
		return depthFrame;
	}
	
	const Mat& Xtion::getData()
	{
		static VideoFrameRef frameColor;
		static VideoStream* stream[] = { &color };
		
		openni::Status rc;
		int changedStream;
		
		rc = OpenNI::waitForAnyStream(stream,1,&changedStream,2000);
		
		if (rc != STATUS_OK)
		{
			WARN("Wait failed (timeout 2000 ms). Error: " << OpenNI::getExtendedError() << "." << endl);
			
			exit(rc);
		}
		
		rc = color.readFrame(&frameColor);
		
		if (rc != STATUS_OK)
		{
			WARN("Reading color matrix failed: " << OpenNI::getExtendedError() << "." << endl);
			
			exit(rc);
		}
		
		frame = Mat(frameColor.getHeight(),frameColor.getWidth(),CV_8UC3,(void*) frameColor.getData());
		
		/// The image has the blue and red channels switched.
		cvtColor(frame,frame,CV_RGB2BGR);
		
		return frame;
	}
}
