/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Drivers/BasicDriver.h>
#include <opencv2/highgui/highgui.hpp>

namespace Drivers
{
	/**
	 * @class Camera
	 * 
	 * @brief Class that implements a driver both for a generic and a remote RGB camera sensor.
	 */
	class Camera : public BasicDriver
	{
		private:
			/**
			 * @brief object that connects to the camera.
			 */
			cv::VideoCapture device;
			
		public:
			/**
			 * @brief Constructor that takes the agent id as initialization value.
			 * 
			 * It initializes the agent id with the one given as input.
			 * 
			 * @param agentId id of the agent.
			 * @param videoFilename path of the video to be opened.
			 * @param isGui \b true means that PInterface is used to run the code, \b false the code has been ran via terminal.
			 */
			Camera(int agentId, const std::string& videoFilename = "", bool isGui = false);
			
			/**
			 * @brief Destructor.
			 */
			~Camera();
			
			/**
			 * @brief Function that reads the RGB image from the sensor.
			 * 
			 * @return the RGB image of the sensor.
			 */
			const cv::Mat& getData();
			
			/**
			 * @brief Function that returns the frame rate of the video.
			 * 
			 * @return the frame rate of the video.
			 */
			inline float getFrameRate() const { return device.get(CV_CAP_PROP_FPS); }
			
			/**
			 * @brief Function that reopens the stream.
			 * 
			 * @param videoFilename path of the video to be opened.
			 */
			void reopen(const std::string& videoFilename);
	};
}
