/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "Camera.h"
#include <Manfield/ConfigFile/ConfigFile.h>
#include <Manfield/Utils/DebugUtils.h>

using namespace std;
using namespace cv;
using GMapping::ConfigFile;

namespace Drivers
{
	Camera::Camera(int agentId, const string& videoFilename, bool isGui)
	{
		ConfigFile fCfg;
		stringstream s;
		string deviceName, key, section;
		bool isRemote;
		
		isRemote = false;
		
		if (videoFilename == "")
		{
			if (!fCfg.read(string(getenv("PTracking_ROOT")) + string("/../config/camera.cfg")))
			{
				ERR("Error reading file '" << string(getenv("PTracking_ROOT")) << string("/../config/camera.cfg") << "' for Camera configuration. Exiting..." << endl);
				
				exit(-1);
			}
			
			try
			{
				s << "Camera_" << agentId;
				
				section = s.str();
				key = "url";
				
				deviceName = string(fCfg.value(section,key));
				
				isRemote = deviceName.find("://") != string::npos;
			}
			catch (...)
			{
				if (isGui) return;
				
				ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
				
				exit(-1);
			}
		}
		else deviceName = videoFilename;
		
		if (isRemote)
		{
			if (!isGui)
			{
				INFO(endl << "Trying to open remote camera at ");
				LOG(deviceName);
				INFO("...");
			}
			
			device.open(deviceName);
			
			if (device.isOpened()) INFO("done!" << endl);
		}
		else if (deviceName.size() == 1) device.open(atoi(deviceName.c_str()));
		else device.open(deviceName.c_str());
		
		if (!device.isOpened())
		{
			if (isGui) return;
			
			ERR("Could not open device: " << deviceName << endl);
			
			exit(-1);
		}
	}
	
	Camera::~Camera() {;}
	
	const Mat& Camera::getData()
	{
		device >> frame;
		
		return frame;
	}
	
	void Camera::reopen(const string& videoFilename)
	{
		device.open(videoFilename.c_str());
	}
}
