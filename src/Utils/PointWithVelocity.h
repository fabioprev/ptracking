/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include "Point2of.h"

namespace PTracking
{
	/**
	 * @class PointWithVelocity
	 * 
	 * @brief Class that represents a cartesian point with orientation and velocity.
	 */
	class PointWithVelocity
	{
		public:
			/**
			 * @brief cartesian point with orientation.
			 */
			Point2of pose;
			
			/**
			 * @brief velocity in the x axis.
			 */
			float vx;
			
			/**
			 * @brief velocity in the y axis.
			 */
			float vy;
			
			/**
			 * @brief Empty constructor.
			 * 
			 * It initializes the cartesian point and the velocities with the default value 0.
			 */
			PointWithVelocity() : pose(), vx(0.0), vy(0.0) {;}
	};
}
