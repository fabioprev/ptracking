/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

namespace PTracking
{
	/**
	 * @class Matrix2f
	 * 
	 * @brief Class that defines a 2d floating matrix.
	 */
	class Matrix2f
	{
		private:
			/**
			 * @brief values of the matrix.
			 */
			float** data;
			
			/**
			 * @brief number of rows.
			 */
			int rows;
			
			/**
			 * @brief number of columns.
			 */
			int columns;
			
		public:
			/**
			 * @brief Constructor that takes the number of rows and columns to create the matrix.
			 * 
			 * It allocates a matrix with the dimension given as input.
			 * 
			 * @param rows number of rows.
			 * @param columns number of columns.
			 * @param initializationValue initialization value of the cell of the matrix.
			 */
			Matrix2f(int rows, int columns, float initializationValue = 0) : rows(rows), columns(columns)
			{
				data = new float*[rows];
				
				for (int i = 0; i < rows; ++i) data[i] = new float[columns];
				
				for (int i = 0; i < rows; ++i)
				{
					for (int j = 0; j < columns; ++j)
					{
						data[i][j] = initializationValue;
					}
				}
			}
			
			/**
			 * @brief Copy constructor.
			 * 
			 * @param m reference to the matrix to be copied.
			 */
			Matrix2f(const Matrix2f& m)
			{
				rows = m.rows;
				columns = m.columns;
				
				data = new float*[rows];
				
				for (int i = 0; i < rows; ++i) data[i] = new float[columns];
				
				for (int i = 0; i < rows; ++i)
				{
					for (int j = 0; j < columns; ++j)
					{
						data[i][j] = m.data[i][j];
					}
				}
			}
			
			/**
			 * @brief Destructor.
			 * 
			 * It deallocates the memory used to store the matrix.
			 */
			virtual ~Matrix2f()
			{
				for (int i = 0; i < rows; ++i) delete[] data[i];
				
				delete[] data;
			}
			
			/**
			 * @brief Function that returns the number of columns of the matrix.
			 * 
			 * @return the number of columns of the matrix.
			 */
			inline int getColumns() const { return columns; }
			
			/**
			 * @brief Function that returns the number of rows of the matrix.
			 * 
			 * @return the number of rows of the matrix.
			 */
			inline int getRows() const { return rows; }
			
			/**
			 * @brief Function that returns a reference to the element in the position specified by i and j.
			 * 
			 * @param i row index.
			 * @param j column index.
			 * 
			 * @return reference to the element in the position specified by i and j.
			 */
			float& operator() (int i, int j)
			{
				return data[i][j];
			}
			
			/**
			 * @brief Function that returns a const reference to the element in the position specified by i and j.
			 * 
			 * @param i row index.
			 * @param j column index.
			 * 
			 * @return const reference to the element in the position specified by i and j.
			 */
			const float& operator() (int i, int j) const
			{
				return data[i][j];
			}
			
			/**
			 * @brief Operator that returns a copy of the object given as input.
			 * 
			 * @param reference to the object that has to be copied.
			 * 
			 * @return copy of the object given as input.
			 */
			Matrix2f& operator= (const Matrix2f& other)
			{
				if (this != &other)
				{
					for (int i = 0; i < rows; ++i) delete[] data[i];
					
					delete[] data;
					
					data = 0;
					
					rows = other.rows;
					columns = other.columns;
					
					data = new float*[rows];
					
					for (int i = 0; i < rows; ++i) data[i] = new float[columns];
					
					for (int i = 0; i < rows; ++i)
					{
						for (int j = 0; j < columns; ++j)
						{
							data[i][j] = other.data[i][j];
						}
					}
				}
				
				return *this;
			}
	};
}
