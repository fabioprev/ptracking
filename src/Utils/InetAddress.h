/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include "SocketAddress.h"
#include <arpa/inet.h>     
#include <string>
#include <sstream>

namespace PTracking
{
	/**
	 * @class InetAddress
	 * 
	 * @brief Class that defines a new version of the \a sockaddr structure.
	 */
	class InetAddress : public SocketAddress
	{
		private:
			/**
			 * @brief address of the object's creator.
			 */
			std::string creationAddress;
			
			/**
			 * @brief string representing the last error occurred.
			 */
			std::string lastError;
			
			/**
			 * @brief value representing if the object is valid or not.
			 */
			bool valid;
			
		public:
			/**
			 * @brief Empty constructor.
			 * 
			 * It initializes address, creationAddress, lastError and valid with the default value.
			 */
			InetAddress();
			
			/**
			 * @brief Constructor that takes a port as initialization value.
			 * 
			 * It initializes the sender/receiver address with the port given as input.
			 * 
			 * @param port sender/receiver port.
			 */
			InetAddress(unsigned short port);
			
			/**
			 * @brief Constructor that takes an address and a port as initialization values.
			 * 
			 * It initializes the sender/receiver address with the values given as input.
			 * 
			 * @param address sender/receiver address.
			 * @param port sender/receiver port.
			 */
			InetAddress(const std::string& address, unsigned short port);
			
			/**
			 * @brief Constructor that takes a \a sockaddr address as initialization value.
			 * 
			 * It initializes the sender/receiver address with the one given as input.
			 * 
			 * @param address sender/receiver address.
			 */
			inline InetAddress(const struct sockaddr& address) : SocketAddress(address) {;}
			
			/**
			 * @brief Constructor that takes a \a sockaddr_in address as initialization value.
			 * 
			 * It initializes the sender/receiver address with the one given as input.
			 * 
			 * @param address sender/receiver address.
			 */
			inline InetAddress(const struct sockaddr_in& address) : SocketAddress((struct sockaddr&) address) {;}
			
			/**
			 * @brief Constructor that takes a SocketAddress as initialization value.
			 * 
			 * It initializes the sender/receiver address with the one given as input.
			 * 
			 * @param address reference to the sender/receiver address.
			 */
			InetAddress(const SocketAddress& address);
			
			/**
			 * @brief Function that returns the address of the sender/receiver.
			 * 
			 * @return the address of the sender/receiver.
			 */
			inline struct sockaddr_in getAddress() const { return (struct sockaddr_in&) address; }
			
			/**
			 * @brief Function that returns the address of the object's creator.
			 * 
			 * @return the object's creator address.
			 */
			inline const std::string getCreationAddress() const { return creationAddress; }
			
			/**
			 * @brief Function that returns the family of the sender/receiver.
			 * 
			 * @return the family of the sender/receiver.
			 */
			inline short getFamily() const { return ((struct sockaddr_in&) address).sin_family; }
			
			/**
			 * @brief Function that returns the IP of the sender/receiver.
			 * 
			 * @return the IP of the sender/receiver.
			 */
			inline std::string getIPAddress() const { return inet_ntoa( ((struct sockaddr_in&) address).sin_addr ); }
			
			/**
			 * @brief Function that returns the last error occurred.
			 * 
			 * @return the last error occurred.
			 */
			inline std::string getLastError() { return lastError; }
			
			/**
			 * @brief Function that returns the port of the sender/receiver.
			 * 
			 * @return the port of the sender/receiver.
			 */
			inline unsigned short getPort() const { return ntohs( ((struct sockaddr_in&) address).sin_port); }
			
			/**
			 * @brief Function that returns the validity of the object.
			 * 
			 * @return \b true if the object is valid, \b false otherwise.
			 */
			inline bool isValid() { return valid; }
			
			/**
			 * @brief Function that updates the address with the one given as input.
			 * 
			 * @param address new sender/receiver address.
			 */
			inline void setAddress(const struct sockaddr& address) { this->address = address; creationAddress = ""; }
			
			/**
			 * @brief Function that updates the address with the one given as input.
			 * 
			 * @param address new sender/receiver address.
			 */
			inline void setAddress(const struct sockaddr_in& address) { this->address = (struct sockaddr&) address; creationAddress = ""; }
			
			/**
			 * @brief Function that updates the address with the one given as input.
			 * 
			 * @param address new sender/receiver address.
			 */
			void setAddress(const std::string& address);
			
			/**
			 * @brief Function that updates the port with the one given as input.
			 * 
			 * @param port new sender/receiver port.
			 */
			inline void setPort(unsigned short port) { ((struct sockaddr_in&) address).sin_port = htons(port); }
			
			/**
			 * @brief Function that converts the sender/receiver address into a string.
			 * 
			 * @return a string representing the sender/receiver address.
			 */
			inline std::string toString() const { std::ostringstream oss; oss << getIPAddress() << ":" << getPort(); return oss.str(); }
			
			/**
			 * @brief Operator that checks if the current address is less than the one given as input.
			 * 
			 * The comparison of the two addresses begins by first checking the IP values and, if equals, then by checking the port values.
			 * 
			 * @param addr address that we want to check.
			 * 
			 * @return \b true if the current address is less than addr, \b false otherwise.
			 */
			bool operator< (const InetAddress& addr) const;
	};
}
