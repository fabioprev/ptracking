/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

namespace PTracking
{
	/**
	 * @brief human-readable typedef of the estimations performed by the team of agents.
	 */
	typedef std::map<int,std::pair<std::pair<PTracking::ObjectSensorReading::Observation,PTracking::Point2f>,std::pair<std::string,int> > > MultiAgentEstimations;
	
	/**
	 * @brief human-readable typedef of the estimations performed by the agent.
	 */
	typedef std::map<int,std::pair<PTracking::ObjectSensorReading::Observation,PTracking::Point2f> > SingleAgentEstimations;
}
