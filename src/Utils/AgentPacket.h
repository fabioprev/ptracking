/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Core/Filters/ObjectSensorReading.h>
#include <Manfield/Filters/GMLocalizer/Structs.h>
#include <Utils/Typedef.h>
#include <Utils/Utils.h>
#include <inttypes.h>

namespace PTracking
{
	/**
	 * @class AgentPacket
	 * 
	 * @brief Class that defines the packet containing the information exchanged between the agents.
	 */
	class AgentPacket
	{
		public:
			/**
			 * @struct Data
			 * 
			 * @brief Struct representing a message exchanged by the team of agents. 
			 */
			struct Data
			{
				/**
				 * @brief sender agent address.
				 */
				std::string ip;
				
				/**
				 * @brief sender agent port.
				 */
				int port;
				
				/**
				 * @brief position of the sender agent.
				 */
				Point2of agentPose;
				
				/**
				 * @brief map of estimations performed by the sender agent (local estimation layer).
				 */
				SingleAgentEstimations estimatedTargetModels;
				
				/**
				 * @brief timestamp of the particles.
				 */
				uint64_t particlesTimestamp;
			};
			
			/**
			 * @brief message containing the information of an agent to send over the network.
			 */
			Data dataPacket;
			
			/**
			 * @brief Empty constructor.
			 */
			AgentPacket() {;}
			
			/**
			 * @brief Destructor.
			 */
			~AgentPacket() {;}
			
			/**
			 * @brief Function that fills the packet fields by parsing the information contained in the string given as input.
			 * 
			 * @param s reference to the information to be sent by an agent (string format).
			 */
			inline void setData(const std::string& s)
			{
				std::stringstream app;
				int size;
				
				app << s;
				
				app >> dataPacket.ip >> dataPacket.port >> dataPacket.agentPose.x >> dataPacket.agentPose.y >> dataPacket.agentPose.theta >> size;
				
				for (int i = 0; i < size; ++i)
				{
					ObjectSensorReading::Observation o;
					int targetIdentity;
					
					app >> targetIdentity >> o.observation.x >> o.observation.y >> o.sigma.x >> o.sigma.y
						>> o.model.width >> o.model.height >> o.model.barycenter >> o.model.velocity.x >> o.model.velocity.y >> o.model.averagedVelocity.x >> o.model.averagedVelocity.y;
					
					dataPacket.estimatedTargetModels.insert(std::make_pair(targetIdentity,std::make_pair(o,o.sigma)));
				}
				
				app >> dataPacket.particlesTimestamp;
			}
			
			/**
			 * @brief Function that converts the packet information into a string.
			 * 
			 * @return a string representing the packet information.
			 */
			inline std::string toString()
			{
				std::stringstream app;
				
				app << dataPacket.ip << " " << dataPacket.port << " " << Utils::roundN(dataPacket.agentPose.x,2) << " "
					<< Utils::roundN(dataPacket.agentPose.y,2) << " " << Utils::roundN(dataPacket.agentPose.theta,2) << " "
					<< dataPacket.estimatedTargetModels.size();
				
				for (SingleAgentEstimations::iterator it = dataPacket.estimatedTargetModels.begin(); it != dataPacket.estimatedTargetModels.end(); ++it)
				{
					app << " " << it->first << " " << Utils::roundN(it->second.first.observation.x,2) << " " << Utils::roundN(it->second.first.observation.y,2)
						<< " " << Utils::roundN(it->second.first.sigma.x,2) << " " << Utils::roundN(it->second.first.sigma.y,2)
						<< " " << it->second.first.model.width << " " << it->second.first.model.height << " " << it->second.first.model.barycenter << " " << it->second.first.model.velocity.x
						<< " " << it->second.first.model.velocity.y << " " << it->second.first.model.averagedVelocity.x << " " << it->second.first.model.averagedVelocity.y;
				}
				
				app << " " << dataPacket.particlesTimestamp;
				
				return app.str();
			}
	};
}
