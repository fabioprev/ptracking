/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "BasicSensorMap.h"
#include <Utils/Utils.h>

namespace PTracking
{
	BasicSensorMap::BasicSensorMap() {;}
	
	BasicSensorMap::~BasicSensorMap() {;}
	
	Point2f BasicSensorMap::getRandomPointInWorld() const
	{
		Point2f point;
		
		point.x = Utils::randrc(2 * getworldMax().x,0.0);
		point.y = Utils::randrc(2 * getworldMax().y,0.0);
		
		return point;
	}
	
	bool BasicSensorMap::isInsideWorld(float x, float y) const
	{
		return ((x >= m_worldMin.x) && (x <= m_worldMax.x) && (y >= m_worldMin.y) && (y <= m_worldMax.y));
	}
}
