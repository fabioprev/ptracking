/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Manfield/SensorModel.h>
#include <Utils/Point2f.h>

namespace PTracking
{
	/**
	 * @class BasicSensorMap
	 * 
	 * @brief Class that implements a model of a map.
	 */
	class BasicSensorMap : public manfield::SensorMap
	{
		public:
			/**
			 * @brief Empty Constructor.
			 */
			BasicSensorMap();
			
			/**
			 * @brief Destructor.
			 */
			~BasicSensorMap();
			
			/**
			 * @brief Function that returns a random point inside the world.
			 * 
			 * @return the random point generated.
			 */
			PTracking::Point2f getRandomPointInWorld() const;
			
			/**
			 * @brief Function that checks if a given point is inside the world.
			 * 
			 * @param x ordinate value of the point to be checked.
			 * @param y abscissa value of the point to be checked.
			 * 
			 * @return \b true if the point is inside the world, \b false otherwise.
			 */
			bool isInsideWorld(float x, float y) const;
			
			/**
			 * @brief Function that inserts a new sensor into the sensor's bank.
			 * 
			 * @param sensorPose reference to the position of the sensor to be added.
			 */
			inline void insertSensor(const PTracking::Point2f& sensorPose) { m_landmarks.push_back(sensorPose); }
			
			/**
			 * @brief Macro that defines the worldMin member variable and its get and set function.
			 */
			PARAM_SET_GET(PTracking::Point2f, worldMin, protected, public, public)
			
			/**
			 * @brief Macro that defines the worldMax member variable and its get and set function.
			 */
			PARAM_SET_GET(PTracking::Point2f, worldMax, protected, public, public)
			
			/**
			 * @brief Macro that defines the landmarks member variable and its get and set function.
			 */
			PARAM_SET_GET(std::vector<PTracking::Point2f>, landmarks, public, public, public)
	};
}
