/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <ThirdParty/GMapping/sensor/sensor_base/sensor.h>

namespace PTracking
{
	/**
	 * @class BasicSensor
	 * 
	 * @brief Class that implements a sensor.
	 */
	class BasicSensor : public GMapping::Sensor
	{
		public:
			/**
			 * @brief Constructor that takes the name of the sensor as initialization value.
			 * 
			 * It initializes the name of the sensor with the one given as input.
			 * 
			 * @param name sensor name.
			 */
			BasicSensor(const std::string& name = "BasicSensor");
			
			/**
			 * @brief Destructor.
			 */
			~BasicSensor();
	};
}
