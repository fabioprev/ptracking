/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Core/Clusterizer/Clusterizer.h>

namespace PTracking
{
	/**
	 * @class QTClusterizer
	 * 
	 * @brief Class that implements the free-clustering algorithm called QT-Clustering.
	 */
	class QTClusterizer : public Clusterizer
	{
		private:
			/**
			 * @brief Function that finds the biggest cluster obtained after the clusterization phase.
			 * 
			 * @param allCandidateClusters reference to the vector of clusters obtained.
			 * @param indexParticlesToEliminate reference to a vector containing the indexes of particles to be deleted, once found the biggest cluster.
			 * 
			 * @return the particles representing the biggest cluster.
			 */
			PoseParticleVector getBiggestCluster(const std::vector<std::vector<std::pair<PoseParticle,int> > >& allCandidateClusters, std::vector<int>& indexParticlesToEliminate) const;
			
			/**
			 * @brief Function that checks if two particles are close each other within a maximum threshold distance.
			 * 
			 * @param p1 reference to the first particle to be checked.
			 * @param p2 reference to the second particle to be checked.
			 * @param qualityThreshold maximum threshold distance.
			 * 
			 * @return \b true if the particles are close each other, \b false otherwise.
			 */
			bool isNear(const PoseParticle& p1, const PoseParticle& p2, float qualityThreshold) const;
			
		public:
			/**
			 * @brief Empty constructor.
			 */
			QTClusterizer();
			
			/**
			 * @brief Destructor.
			 */
			~QTClusterizer();
			
			/**
			 * @brief Function that clusterizes a vector of particles creating a set of clusters with a maximum radius given as input.
			 * 
			 * @param particleVector reference to the particles' vector that have to be clusterized.
			 * @param qualityThreshold maximum radius of a cluster.
			 */
			void clusterize(const PoseParticleVector& particleVector, float qualityThreshold);
	};
}
