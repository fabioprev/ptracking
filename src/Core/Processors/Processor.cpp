/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "Processor.h"
#include <Core/Filters/ObjectSensorReading.h>
#include <Manfield/Utils/DebugUtils.h>
#include <Utils/Utils.h>

using namespace std;
using GMapping::SensorReading;

namespace PTracking
{
	// In seconds.
	static const float UPDATE_FREQUENCY = 1;
	
	Processor::Processor() : ManifoldFilterProcessor(), m_updateFrequency(UPDATE_FREQUENCY), m_nFusedParticles(0) {;}
	
	Processor::~Processor() {;}
	
	void Processor::processReading(const Point2of& robotPose, const Timestamp& initialTimestamp, const Timestamp& currentTimestamp, vector<ObjectSensorReading>& readings)
	{
		if (m_first)
		{
			m_first = false;
			m_bootstrapRequired = true;
		}
		
		if (updateNeeded(robotPose,currentTimestamp))
		{
			updateBootStrap();
			
			for (FilterBank::const_iterator it = m_filterBank.begin(); it != m_filterBank.end(); it++)
			{
				singleFilterIteration(robotPose,*static_cast<ObjectParticleFilter*>(it->second),initialTimestamp,currentTimestamp,readings);
			}
			
			// Resetting clock.
			lastRobotPose = robotPose;
			timeOfLastIteration.setToNow();
		}
	}
	
	void Processor::singleFilterIteration(const Point2of& robotPose, ObjectParticleFilter& f, const Timestamp& initialTimestamp, const Timestamp& currentTimestamp, vector<ObjectSensorReading>& readings) const
	{
		f.predict(robotPose,lastRobotPose,initialTimestamp,currentTimestamp);
		
		for (vector<ObjectSensorReading>::iterator it = readings.begin(); it != readings.end(); it++)
		{
			if (it->getSensor().getName() == f.getSensor().getName())
			{
				f.observe(*it);
			}
		}
	}
	
	bool Processor::updateNeeded(const Point2of& robotPose, const Timestamp& currentTimestamp) const
	{
		if ((currentTimestamp - timeOfLastIteration).getMs() > MAX_TIME_TO_WAIT) return true;
		
		if (fabs(robotPose.mod() - lastRobotPose.mod()) > 0.03) return true;
		
		if (Utils::rad2deg(fabs(robotPose.theta - lastRobotPose.theta)) > 5.0) return true;
		
		return false;
	}
}
