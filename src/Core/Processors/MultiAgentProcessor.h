/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Core/Filters/ObjectParticleFilterMultiAgent.h>
#include <Manfield/ManifoldProcessor.h>
#include <Utils/Timestamp.h>

namespace PTracking
{
	/**
	 * @class MultiAgentProcessor
	 * 
	 * @brief Class that implements a multi agent processor.
	 */
	class MultiAgentProcessor : public manfield::ManifoldFilterProcessor
	{
		private:
			/**
			 * @brief timestamp of the last iteration.
			 */
			Timestamp timeOfLastIteration;
			
		public:
			/**
			 * @brief Empty constructor.
			 */
			MultiAgentProcessor();
			
			/**
			 * @brief Destructor.
			 */
			~MultiAgentProcessor();
			
			/**
			 * @brief Function that initializes several configuration parameters.
			 */
			void init();
			
			/**
			 * @brief Function that processes, if needed, the estimations received by the other agents between the previous and current iteration.
			 * 
			 * @param readings reference to the estimations received by the other agents between the previous and current iteration.
			 */
			void processReading(const std::vector<ObjectSensorReadingMultiAgent>& readings);
			
			/**
			 * @brief Function that invokes the predict and update step of the underlying particle filter.
			 * 
			 * @param f reference to the underlying particle filter.
			 * @param readings reference to the estimations received by the other agents between the previous and current iteration.
			 */
			void singleFilterIteration(ObjectParticleFilterMultiAgent& f, const vector<ObjectSensorReadingMultiAgent>& readings) const;
			
			/**
			 * @brief Function that checks if the processing step is needed.
			 * 
			 * @return \b true if the processing step is needed, \b false otherwise.
			 */
			bool updateNeeded() const;
			
			PARAM_SET_GET(float, updateFrequency, private, public, public)
			PARAM_SET_GET(unsigned int, nFusedParticles, private, public, public)
	};
}
