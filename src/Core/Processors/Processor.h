/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Core/Filters/ObjectParticleFilter.h>
#include <Manfield/ManifoldProcessor.h>

namespace PTracking
{
	/**
	 * @class Processor
	 * 
	 * @brief Class that implements a single agent processor.
	 */
	class Processor : public manfield::ManifoldFilterProcessor
	{
		protected:
			/**
			 * @brief value representing the maximum time (in milliseconds) to wait before executing a processing step.
			 */
			static constexpr float MAX_TIME_TO_WAIT = 0.0;
			
			/**
			 * @brief last position of the robot.
			 */
			Point2of lastRobotPose;
			
			/**
			 * @brief timestamp of the last iteration.
			 */
			Timestamp timeOfLastIteration;
			
		public:
			/**
			 * @brief Empty constructor.
			 */
			Processor();
			
			/**
			 * @brief Destructor.
			 */
			~Processor();
			
			/**
			 * @brief Function that processes, if needed, the observations gathered from the sensors between the previous and current iteration.
			 * 
			 * @param robotPose reference to the position of the robot when the observations have been acquired.
			 * @param initialTimestamp reference to the timestamp of the previous iteration.
			 * @param currentTimestamp reference to the timestamp of the current iteration.
			 * @param readings reference to the observations gathered from the sensors between the previous and current iteration.
			 */
			void processReading(const Point2of& robotPose, const Timestamp& initialTimestamp, const Timestamp& currentTimestamp, std::vector<ObjectSensorReading>& readings);
			
			/**
			 * @brief Function that invokes the predict and update step of the underlying particle filter.
			 * 
			 * @param robotPose reference to the position of the robot when the observations have been acquired.
			 * @param f reference to the underlying particle filter.
			 * @param initialTimestamp reference to the timestamp of the previous iteration.
			 * @param currentTimestamp reference to the timestamp of the current iteration.
			 * @param readings reference to the observations gathered from the sensors between the previous and current iteration.
			 */
			void singleFilterIteration(const Point2of& robotPose, ObjectParticleFilter& f, const Timestamp& initialTimestamp, const Timestamp& currentTimestamp, vector<ObjectSensorReading>& readings) const;
			
			/**
			 * @brief Function that checks if the processing step is needed (i.e. a time of MAX_TIME_TO_WAIT is passed or the robot sufficiently moved either in translation or orientation).
			 * 
			 * @param robotPose reference to the current position of the robot.
			 * @param currentTimestamp reference to the timestamp of the current iteration.
			 * 
			 * @return \b true if the processing step is needed, \b false otherwise.
			 */
			bool updateNeeded(const Point2of& robotPose, const Timestamp& currentTimestamp) const;
			
			PARAM_SET_GET(float, updateFrequency, private, public, public)
			PARAM_SET_GET(unsigned int, nFusedParticles, private, public, public)
	};
}
