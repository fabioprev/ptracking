/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include "ObjectSensorReading.h"
#include <Utils/Typedef.h>

namespace PTracking
{
/**
	 * @class GroupTracking
	 * 
	 * @brief Class that implements the tracking of a group of estimations.
	 */
	class GroupTracking
	{
		public:
			/**
			 * @brief Pair containing the averaged velocity of the two estimations just before entering the group.
			 */
			std::pair<Point2f,Point2f> averagedVelocity;
			
			/**
			 * @brief Pair containing the instant velocity of the two estimations just before entering the group.
			 */
			std::pair<Point2f,Point2f> instantVelocity;
			
			/**
			 * @brief Mixed velocity (average and instant) of the two estimations. Computed as (instantVelocity * 2 + averagedVelocity) / 3.
			 */
			std::pair<Point2f,Point2f> mixedVelocity;
			
			/**
			 * @brief Pair of identities of the estimations belonging to the group.
			 */
			std::pair<int,int> groupEstimations;
			
			/**
			 * @brief Position of the group computed as the average of the observations of the two estimations.
			 */
			Point2f position;
			
			/**
			 * @brief Function that apply the group tracking.
			 * 
			 * @param estimatedTargetModelsWithIdentity map of estimations having both an identity and a model performed by the agent.
			 * 
			 * @return the map of the group estimations having both an identity and a model performed by the agent.
			 */
			std::vector<std::pair<std::string,std::pair<ObjectSensorReading::Observation,Point2f> > > exec(SingleAgentEstimations estimatedTargetModelsWithIdentity);
	};
}
