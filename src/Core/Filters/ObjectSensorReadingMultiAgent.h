/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include "ObjectSensorReading.h"
#include <Core/Sensors/BasicSensor.h>
#include <Manfield/Filters/GMLocalizer/Structs.h>
#include <Utils/Point2f.h>
#include <Utils/Typedef.h>
#include <ThirdParty/GMapping/sensor/sensor_base/sensorreading.h>

namespace PTracking
{
	/**
	 * @class ObjectSensorReadingMultiAgent
	 * 
	 * @brief Class that defines the observations exchanged by the team of agents.
	 */
	class ObjectSensorReadingMultiAgent : public GMapping::SensorReading
	{
		public:
			/**
			 * @struct ObservationMultiAgent
			 * 
			 * @brief Struct representing an estimation exchanged by the team of agents. 
			 */
			struct ObservationMultiAgent
			{
				/**
				 * @brief vector of estimations with identity performed by an agent.
				 */
				SingleAgentEstimations estimationsWithModels;
				
				/**
				 * @brief address of the agent.
				 */
				std::string address;
				
				/**
				 * @brief port of the agent.
				 */
				int port;
				
				/**
				 * @brief timestamp of the estimations.
				 */
				unsigned long timestamp;
			};
			
			/**
			 * @brief Empty constructor.
			 */
			ObjectSensorReadingMultiAgent();
			
			/**
			 * @brief Destructor.
			 */
			~ObjectSensorReadingMultiAgent();
			
			/**
			 * @brief Function that returns the address and port of the agent.
			 * 
			 * @return the address and port of the agent.
			 */
			inline std::pair<std::string,int> getAgent() const { return std::make_pair(observationMultiAgent.address,observationMultiAgent.port); }
			
			/**
			 * @brief Function that returns the timestamp of the estimations.
			 * 
			 * @return the timestamp of the estimations.
			 */
			inline unsigned long getEstimationsTimestamp() const { return observationMultiAgent.timestamp; }
			
			/**
			 * @brief Function that returns the estimations performed by the team of agents.
			 * 
			 * @return a reference to the vector of estimations performed by the team of agents.
			 */
			inline const SingleAgentEstimations& getEstimationsWithModels() const { return observationMultiAgent.estimationsWithModels; }
			
			/**
			 * @brief Function that returns the sensor used by the team of agents.
			 * 
			 * @return a reference to the sensor used by the team of agents.
			 */
			inline const BasicSensor& getSensor() const { return sensor; }
			
			/**
			 * @brief Function that updates the address and the port of the agent.
			 * 
			 * @param address sender address.
			 * @param port sender port.
			 */
			void setAgent(const std::string& address, int port);
			
			/**
			 * @brief Function that updates the timestamp of the estimations.
			 * 
			 * @param timestamp new timestamp of the estimations.
			 */
			void setEstimationsTimestamp(unsigned long timestamp);
			
			/**
			 * @brief Function that updates the estimations performed by the team of agents.
			 * 
			 * @param estimationsWithModels reference to a new vector of estimations.
			 */
			void setEstimationsWithModels(const SingleAgentEstimations& estimationsWithModels);
			
			/**
			 * @brief Function that sets a new sensor.
			 * 
			 * @param s reference to a new sensor.
			 */
			void setSensor(const BasicSensor& s);
			
		private:
			/**
			 * @brief estimations performed by an agent.
			 */
			ObservationMultiAgent observationMultiAgent;
			
			/**
			 * @brief sensor used by the team of agents.
			 */
			BasicSensor sensor;
	};
}
