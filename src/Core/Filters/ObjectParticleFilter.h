/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include "GroupTracking.h"
#include "ObjectSensorReading.h"
#include <Manfield/Filters/ParticleFilter.h>
#include <Utils/Timestamp.h>
#include <Utils/Typedef.h>

namespace PTracking
{
	/**
	 * @brief Forward declaration.
	 */
	class Clusterizer;
}

namespace PTracking
{
	/**
	 * @class ObjectParticleFilter
	 * 
	 * @brief Class that implements the particle filter used in the local estimation layer.
	 */
	class ObjectParticleFilter : public manfield::ParticleFilter
	{
		private:
			/**
			 * @brief map of estimations having both an identity and a model performed by the agent.
			 */
			SingleAgentEstimations estimatedTargetModelsWithIdentity;
			
			/**
			 * @brief map of the group estimations having both an identity and a model performed by the agent.
			 */
			std::vector<std::pair<std::string,std::pair<ObjectSensorReading::Observation,Point2f> > > groupEstimatedTargetModelsWithIdentity;
			
			/**
			 * @brief map of the movements between two frames of the estimations having both an identity and a model performed by the agent.
			 */
			std::map<int,Point2f> estimatedTargetMovements;
			
			/**
			 * @brief map representing the timestamp on which an estimation has been updated.
			 */
			std::map<int,Timestamp> estimationsUpdateTime;
			
			/**
			 * @brief vector representing all the models of the estimations performed by the agent.
			 */
			std::vector<std::pair<ObjectSensorReading::Model,int> > targetModels;
			
			/**
			 * @brief vector of pending observations.
			 */
			std::vector<std::pair<Point2of,std::pair<Timestamp,Timestamp> > > pendingObservations;
			
			/**
			 * @brief map representing the mapping between observations and estimations (used both for updating the observation model and for debugging).
			 */
			std::map<int,std::pair<ObjectSensorReading::Observation,Point2of> > observationsMapping;
			
			/**
			 * @brief averaged linear velocities of the objects (expressed either in m/s or in pixels/s).
			 */
			std::map<int,std::list<std::pair<float,float> > > averagedVelocities;
			
			/**
			 * @brief pointer to the clustering algorithm.
			 */
			Clusterizer* clusterizer;
			
			/**
			 * @brief sub-module for handling group tracking of two moving objects getting close each other.
			 */
			GroupTracking groupTracking;
			
			/**
			 * @brief timestamp of the last time when the target's number should be decreased.
			 */
			Timestamp lastTimeShouldBeDecreased;
			
			/**
			 * @brief type of the clustering algortithm.
			 */
			std::string clusteringAlgorithm;
			
			/**
			 * @brief threshold used for the data association phase.
			 */
			float closenessThreshold;
			
			/**
			 * @brief threshold used to check whether a new estimation is close to a group.
			 */
			float groupClosenessThreshold;
			
			/**
			 * @brief delta time between two iterations.
			 */
			float dt;
			
			/**
			 * @brief maximum linear velocity admissible for an object (expressed either in m/s or in pixels/s).
			 */
            float modelLinearVelocity;
			
			/**
			 * @brief size of the object's model for the clustering algorithm.
			 */
			float modelSize;
			
			/**
			 * @brief velocityStabilizationFactor used to smoothly calculate the velocity of the object.
			 */
			float velocityStabilizationFactor;
			
			/**
			 * @brief timestamp of the current iteration.
			 */
			unsigned long currentTimestamp;
			
			/**
			 * @brief timestamp of the last execution of the observe function.
			 */
			unsigned long lastObserveTimestamp;
			
			/**
			 * @brief window of the averaged velocity of the estimations.
			 */
			unsigned int averagedVelocityWindow;
			
			/**
			 * @brief frame to wait before deleting an estimation no longer associated to an observation coming from the agent's sensors.
			 */
			unsigned int frameToWaitBeforeDeleting;
			
			/**
			 * @brief frame to wait before promoting an observation coming from the agent's sensors.
			 */
			unsigned int frameToWaitBeforePromoting;
			
			/**
			 * @brief number of the observations provided by the agent's sensors that have been either associated to an existing cluster or definitely promoted.
			 */
			unsigned int numberOfObservationAssociatedAndPromoted;
			
			/**
			 * @brief time to wait before deleting an estimation no longer associated to an observation coming from the agent's sensors. It takes into account the current frame rate.
			 */
			unsigned int timeToWaitBeforeDeleting;
			
			/**
			 * @brief time to wait before promoting an observation coming from the agent's sensors. It takes into account the current frame rate.
			 */
			unsigned int timeToWaitBeforePromoting;
			
			/**
			 * @brief maximum identity value assigned to an estimation.
			 */
			int maxIdentityNumber;
			
			/**
			 * @brief true means that the observe step is needed, otherwise it is not needed yet.
			 */
			bool observeNeeded;
			
			/**
			 * @brief if enabled, the tracking will provide in output calibrated data.
			 */
			bool calibrateData;
			
			/**
			 * @brief Function that adds particles for representing a new observation once promoted.
			 * 
			 * @param readings reference to the observations gathered from the sensors between the previous and current iteration.
			 */
			void addParticlesInInterestingPoints(ObjectSensorReading& readings);
			
			/**
			 * @brief Function that checks if two models are similar.
			 * 
			 * @param observationModel reference to the model of the observation.
			 * @param storedModel reference to a model already present in the bank of models.
			 * 
			 * @return \b true if the models are similar, \b false otherwise.
			 */
			std::pair<bool,float> areModelsSimilar(const ObjectSensorReading::Model& observationModel, const ObjectSensorReading::Model& storedModel) const;
			
			/**
			 * @brief Function that deletes an estimation if completely contained by another bigger estimation (in terms of bounding boxes).
			 */
			void deleteConcentricEstimations();
			
			/**
			 * @brief Function that normalizes the weight of the particles.
			 */
			void normalizeWeight();
			
			/**
			 * @brief Function that resamples the particles.
			 */
			void resample();
			
			/**
			 * @brief Function that resets the weight of the particles by setting their value to 1.
			 */
			void resetWeight();
			
			/**
			 * @brief Function that updates the estimations having both an identity and a model performed by the agent.
			 * 
			 * @param readings reference to the observations gathered from the sensors between the previous and current iteration.
			 */
			void updateTargetIdentity(ObjectSensorReading& readings);
			
		public:
			/**
			 * @brief Constructor that takes the type of the particle filter as initialization value.
			 * 
			 * It initializes the type of the particle filter with the one given as input.
			 * 
			 * @param type particle filter type.
			 */
			ObjectParticleFilter(const std::string& type = "ObjectParticleFilter");
			
			/**
			 * @brief Destructor.
			 * 
			 * It deallocates the memory of the clusterizer.
			 */
			~ObjectParticleFilter();
			
			/**
			 * @brief Function that checks whether a re-initialization of the particle filter is needed.
			 * 
			 * @param isHardReset true means that the filter is reset, false means that the filter is reset if and only if it is needed.
			 * 
			 * @return \b true if the re-initialization has been performed, \b false otherwise.
			 */
			bool checkFilterForReinitialization(bool isHardReset = false);
			
			/**
			 * @brief Function that reads a config file in order to initialize several configuration parameters.
			 * 
			 * @param filename file to be read.
			 * @param frameRate number of frames per second.
			 */
			void configure(const std::string& filename, float frameRate);
			
			/**
			 * @brief Function that returns the pointer to the clustering algorithm.
			 * 
			 * @return the pointer to the clustering algorithm.
			 */
			inline Clusterizer* getClusterizer() const { return clusterizer; }
			
			/**
			 * @brief Function that returns the mapping between observations and estimations (used both for updating the observation model and for debugging).
			 * 
			 * @return the mapping between observations and estimations.
			 */
			inline const std::map<int,std::pair<ObjectSensorReading::Observation,Point2of> >& getObservationsMapping() const { return observationsMapping; }
			
			/**
			 * @brief Function that returns the estimations having both an identity and a model performed by the agent.
			 * 
			 * @return the map of estimations having both an identity and a model performed by the agent.
			 */
			inline SingleAgentEstimations getEstimationsWithModel() const { return estimatedTargetModelsWithIdentity; }
			
			/**
			 * @brief Function that returns the group estimations having both an identity and a model performed by the agent.
			 * 
			 * @return the vector of the group estimations having both an identity and a model performed by the agent.
			 */
			inline std::vector<std::pair<std::string,std::pair<ObjectSensorReading::Observation,Point2f> > > getGroupEstimationsWithModel() const { return groupEstimatedTargetModelsWithIdentity; }
			
			/**
			 * @brief Function that returns the sensor used by the agent.
			 * 
			 * @return a reference to the sensor used by the agent.
			 */
			inline const BasicSensor& getSensor() const { return *static_cast<const BasicSensor*>(ParticleFilter::getSensor()); }
			
			/**
			 * @brief Function that creates the new likelihood distribution using the observations gathered from the agent's sensors.
			 * 
			 * @param readings reference to the observations gathered from the sensors between the previous and current iteration.
			 */
			void observe(ObjectSensorReading& readings);
			
			/**
			 * @brief Function that updates the motion model of the particle filter.
			 * 
			 * @param newRobotPose reference to the position of the robot when the observations have been acquired.
			 * @param oldRobotPose reference to the position of the robot in the previous iteration.
			 * @param initialTimestamp reference to the timestamp of the previous iteration.
			 * @param current reference to the timestamp of the current iteration.
			 */
			void predict(const Point2of& newRobotPose, const Point2of& oldRobotPose, const Timestamp& initialTimestamp, const Timestamp& current);
			
			/**
			 * @brief Function that updates the time to wait before deleting an estimation no longer associated to an observation coming from the agent's sensors, based on the current frame rate.
			 * 
			 * @param frameRate number of frames per second.
			 */
			void updateTimeToWaitBeforeDeleting(float frameRate);
			
			/**
			 * @brief Function that updates the time to wait before promoting an observation coming from the agent's sensors, based on the current frame rate.
			 * 
			 * @param frameRate number of frames per second.
			 */
			void updateTimeToWaitBeforePromoting(float frameRate);
			
			/**
			 * @brief Macro that defines the default clone function.
			 */
			FILTER_DEFAULT_CLONE(ObjectParticleFilter)
	};
}
