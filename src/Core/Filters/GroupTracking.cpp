/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "GroupTracking.h"
#include <cfloat>
#include <sstream>

using namespace std;

namespace PTracking
{
	vector<pair<string,pair<ObjectSensorReading::Observation,Point2f> > > GroupTracking::exec(SingleAgentEstimations estimatedTargetModelsWithIdentity)
	{
		vector<pair<string,pair<ObjectSensorReading::Observation,Point2f> > > groupEstimatedTargetModelsWithIdentity;
		vector<pair<int,pair<ObjectSensorReading::Observation,Point2f> > > overlappingEstimatedTargetModels;
		stringstream s;	
		float boundingBoxOverlappingFactor, x1, x2, y1, y2, width1, width2, height1, height2;
		
		boundingBoxOverlappingFactor = 0.8;
		
		/// Checking whether multiple objects have to be merged in a group.
		for (SingleAgentEstimations::iterator it = estimatedTargetModelsWithIdentity.begin(); it != estimatedTargetModelsWithIdentity.end(); )
		{
			SingleAgentEstimations::iterator it2;
			
			it2 = it;
			advance(it2,1);
			
			overlappingEstimatedTargetModels.clear();
			s.clear();
			s.str("");
			
			for (; it2 != estimatedTargetModelsWithIdentity.end(); )
			{
				x1 = it->second.first.observation.x - (it->second.first.model.width / 2.0) + ((1.0 - boundingBoxOverlappingFactor) * (it->second.first.model.width / 2.0));
				y1 = it->second.first.observation.y - it->second.first.model.height + ((1.0 - boundingBoxOverlappingFactor) * (it->second.first.model.height / 2.0));
				x2 = it2->second.first.observation.x - (it2->second.first.model.width / 2.0) + ((1.0 - boundingBoxOverlappingFactor) * (it2->second.first.model.width / 2.0));
				y2 = it2->second.first.observation.y - it2->second.first.model.height + ((1.0 - boundingBoxOverlappingFactor) * (it2->second.first.model.height / 2.0));
				
				width1 = it->second.first.model.width * boundingBoxOverlappingFactor;
				width2 = it2->second.first.model.width * boundingBoxOverlappingFactor;
				
				height1 = it->second.first.model.height * boundingBoxOverlappingFactor;
				height2 = it2->second.first.model.height * boundingBoxOverlappingFactor;
				
				if ((x1 + width1 < x2) || (x2 + width2 < x1) || (y1 + height1 < y2) || (y2 + height2 < y1)) ++it2;
				// The two bounding boxes overlap.
				else
				{
					if (it->first != it2->first)
					{
						groupEstimations = make_pair(it->first,it2->first);
						
						position.x = (it->second.first.observation.x+it2->second.first.observation.x)/2;
						position.y = (it->second.first.observation.y+it2->second.first.observation.y)/2;
						
						averagedVelocity = make_pair(it->second.first.model.averagedVelocity,it2->second.first.model.averagedVelocity);
						instantVelocity  = make_pair(it->second.first.model.velocity,it2->second.first.model.velocity);
						mixedVelocity.first.x = (instantVelocity.first.x * 2 + averagedVelocity.first.x) / 3;
						mixedVelocity.first.y = (instantVelocity.first.y * 2 + averagedVelocity.first.y) / 3;
						mixedVelocity.second.x = (instantVelocity.second.x * 2 + averagedVelocity.second.x) / 3;
						mixedVelocity.second.y = (instantVelocity.second.y * 2 + averagedVelocity.second.y) / 3;
					}
					
					overlappingEstimatedTargetModels.push_back(*it2);
					estimatedTargetModelsWithIdentity.erase(it2++);
				}
			}
			
			if (!overlappingEstimatedTargetModels.empty())
			{
				Point2f groupSigma;
				float barycenter, heightMax, heightMin, widthMax, widthMin;
				
				overlappingEstimatedTargetModels.push_back(*it);
				
				widthMax = -FLT_MAX;
				widthMin = FLT_MAX;
				heightMax = -FLT_MAX;
				heightMin = FLT_MAX;
				
				for (vector<pair<int,pair<ObjectSensorReading::Observation,Point2f> > >::const_iterator it2 = overlappingEstimatedTargetModels.begin(); it2 != overlappingEstimatedTargetModels.end(); ++it2)
				{
					s << it2->first << " ";
					
					x1 = it2->second.first.observation.x;
					y1 = it2->second.first.observation.y;
					
					if ((x1 - ((it2->second.first.model.width / 2.0) * boundingBoxOverlappingFactor)) < widthMin) widthMin = x1 - ((it2->second.first.model.width / 2.0) * boundingBoxOverlappingFactor);
					if ((x1 + ((it2->second.first.model.width / 2.0) * boundingBoxOverlappingFactor)) > widthMax) widthMax = x1 + ((it2->second.first.model.width / 2.0) * boundingBoxOverlappingFactor);
					
					if ((y1 - (it2->second.first.model.height * boundingBoxOverlappingFactor)) < heightMin) heightMin = y1 - (it2->second.first.model.height * boundingBoxOverlappingFactor);
					if ((y1 * boundingBoxOverlappingFactor) > heightMax) heightMax = y1 * boundingBoxOverlappingFactor;
					
					groupSigma.x = it2->second.second.x;
					groupSigma.y = it2->second.second.y;
				}
				
				barycenter = widthMin + ((widthMax - widthMin) / 2.0);
				
				ObjectSensorReading::Observation groupEstimation;
				
				groupEstimation.observation.x = barycenter;
				groupEstimation.observation.y = heightMax;
				groupEstimation.model.barycenter = barycenter;
				groupEstimation.model.height = heightMax - heightMin;
				groupEstimation.model.width = widthMax - widthMin;
				
				groupEstimatedTargetModelsWithIdentity.push_back(make_pair(s.str().substr(0,s.str().size() - 1),make_pair(groupEstimation,groupSigma / overlappingEstimatedTargetModels.size())));
				
				estimatedTargetModelsWithIdentity.erase(it++);
			}
			else
			{
				s << it->first;
				
				groupEstimatedTargetModelsWithIdentity.push_back(make_pair(s.str(),it->second));
				
				++it;
			}
		}
		
		return groupEstimatedTargetModelsWithIdentity;
	}
}
