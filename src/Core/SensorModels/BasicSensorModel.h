/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Core/Filters/ObjectSensorReading.h>
#include <Core/SensorMaps/BasicSensorMap.h>
#include <Utils/Point2of.h>

namespace PTracking
{
	/**
	 * @class BasicSensorModel
	 * 
	 * @brief Class that implements a sensor model.
	 */
	class BasicSensorModel : public manfield::SensorModel
	{
		private:
			/**
			 * @brief value representing the linear velocity (expressed either in m/s or in pixels/s) of an object.
			 */
			float modelLinearVelocity;
			
			/**
			 * @brief value representing the linear sigma of the distribution.
			 */
			float sigmaRho;
			
			/**
			 * @brief value representing the angular sigma of the distribution.
			 */
			float sigmaTheta;
			
			/**
			 * @brief value representing the current linear velocity (expressed either in m/s or in pixels/s) of the target.
			 */
			float linearVelocity;
			
		public:
			/**
			 * @brief Constructor that takes the sensor model type as initialization value.
			 * 
			 * It initializes the type of the sensor model with the one given as input.
			 * 
			 * @param type sensor model type.
			 */
			BasicSensorModel(const std::string& type = "BasicSensorModel");
			
			/**
			 * @brief Destructor.
			 */
			virtual ~BasicSensorModel();
			
			/**
			 * @brief Function that computes the current linear velocity of the target.
			 * 
			 * @param initialTimestamp time when the (i-1)-th target estimation has been performed.
			 * @param currentTimestamp time when the i-th target estimation has been performed.
			 */
			void calculateLinearVelocity(unsigned long initialTimestamp, unsigned long currentTimestamp);
			
			/**
			 * @brief Function that reads a config file in order to initialize several configuration parameters.
			 * 
			 * @param filename file to be read.
			 * @param sensor pointer to the sensor to be used.
			 */
			virtual void configure(const std::string& filename, GMapping::Sensor* sensor = 0);
			
			/**
			 * @brief Function that updates the particles' weight by calculating the new likelihood distribution given a set of observations.
			 * 
			 * @param reading pointer to the current observations.
			 * @param particlesBegin reference to the first particle of the distribution.
			 * @param particlesEnd reference to the last particle of the distribution.
			 */
			virtual void likelihood(GMapping::SensorReading* reading, PointIterator& particlesBegin, PointIterator& particlesEnd) const;
			
			/**
			 * @brief Function that computes the weight of a particle given a set of observations.
			 * 
			 * @param map pointer to the model of the map.
			 * @param pose reference to the cartesian position of the particle.
			 * @param observations reference to the current observations.
			 * 
			 * @return the new weight of the particle.
			 */
			virtual float likelihood(BasicSensorMap* map, PointWithVelocity& pose, const std::vector<ObjectSensorReading::Observation>& observations) const;
			
			/**
			 * @brief Macro that defines the default clone function.
			 */
			MODEL_DEFAULT_CLONE(BasicSensorModel)
	};
}
