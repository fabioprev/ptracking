/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "PTaLer.h"
#include <Detectors/HOG/HOG.h>
#include <Drivers/Camera/Camera.h>
#include <Manfield/ConfigFile/ConfigFile.h>
#include <opencv2/highgui/highgui.hpp>
#include <unistd.h>

#ifdef Imbs_FOUND
	#include <Detectors/Imbs/Imbs.h>
#endif

#ifdef OpenNI2_FOUND
	#include <Drivers/Xtion/Xtion.h>
#endif

#ifdef Freenect_FOUND
	#include <Drivers/Kinect/Kinect.h>
#endif

using namespace std;
using namespace cv;
using namespace PTracking;
using namespace Detectors;
using namespace Drivers;
using namespace Gnuplot;
using GMapping::ConfigFile;

PTaLer::PTaLer(float frameRate, int agentId, const string& filename, const string& calibrationDirectory) : pTracker(agentId,filename,calibrationDirectory,frameRate),
																										   calibrationDirectory(calibrationDirectory), frameRate(frameRate), agentId(agentId)
{
	ConfigFile fCfg;
	string key, section;
	
	if (getenv("PTracking_ROOT") == 0)
	{
		ERR("PTracking_ROOT has NOT been set. Did you log out before running any executable of the PTracking library? Exiting..." << endl);
		
		exit(-1);
	}
	
	if (!fCfg.read(string(getenv("PTracking_ROOT")) + string("/../config/detector.cfg")))
	{
		ERR("Error reading file '" << string(getenv("PTracking_ROOT")) + string("/../config/detector.cfg") << "' for PTaLer configuration. Exiting..." << endl);
		
		exit(-1);
	}
	
	try
	{
		section = "Detector";
		
		key = "name";
		detectorType = string(fCfg.value(section,key));
	}
	catch (...)
	{
		ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
		
		exit(-1);
	}
	
	/// Generating color map for drawing the PTracker estimations.
	for (int i = 0, counter = 1; i < 256; i += 63)
	{
		for (int j = 0; j < 256; j += 63)
		{
			for (int k = 0; k < 256; k += 63, ++counter)
			{
				colorMap.insert(make_pair(counter,make_pair(i,make_pair(j,k))));
			}
		}
	}
}

PTaLer::~PTaLer() {;}

void PTaLer::draw(const Mat& frame, const PTracking::SingleAgentEstimations& estimations, float fps, const string& frameRateCounter, const Mat& planView, const PTracking::MultiAgentEstimations& globalEstimations) const
{
	Mat frameTracking, planViewTracking;
	CvScalar color;
	cv::Point textOrigin;
	stringstream text;
	float imageX, imageY, fontScale;
	int font, height, thickness, width;
	
	font = FONT_HERSHEY_SIMPLEX;
	
	frameTracking = frame.clone();
	planViewTracking = planView.clone();
	
	const SingleAgentEstimations& uncalibratedEstimations = pTracker.uncalibrate(estimations);
	
	/// Local estimations.
	for (SingleAgentEstimations::const_iterator it = uncalibratedEstimations.begin(); it != uncalibratedEstimations.end(); ++it)
	{
		imageX = it->second.first.observation.x;
		imageY = it->second.first.observation.y;
		
		width = it->second.first.model.width;
		height = it->second.first.model.height;
		
		const map<int,pair<int,pair<int,int> > >::const_iterator& colorTrack = colorMap.find(it->first);
		
		color = cvScalar(colorTrack->second.first,colorTrack->second.second.first,colorTrack->second.second.second);
		
		rectangle(frameTracking,cvPoint(imageX - (width / 2),imageY - height),cvPoint(imageX + (width / 2),imageY),color,3);
		
		text.str("");
		text.clear();
		
		text << it->first;
		
		fontScale = 1;
		thickness = 3;
		
		textOrigin.x = imageX - (width / 2);
		textOrigin.y = imageY - height - 7;
		
		putText(frameTracking,text.str(),textOrigin,font,fontScale,Scalar::all(0),thickness,8);
		
		fontScale = 1;
		thickness = 2;
		
		textOrigin.x = imageX - (width / 2) + 2;
		textOrigin.y = imageY - height - 9;
		
		putText(frameTracking,text.str(),textOrigin,font,fontScale,Scalar::all(255),thickness,8);
	}
	
	text.str("");
	text.clear();
	
	text << frameTracking.cols << "x" << frameTracking.rows << ", ";
	
	if (frameRateCounter != "") text << "#" << frameRateCounter << ", ";
	
	text << "FPS: " << fps;
	
	fontScale = 0.55;
	thickness = 2;
	
	textOrigin.x = 10;
	textOrigin.y = 25;
	
	putText(frameTracking,text.str(),textOrigin,font,fontScale,Scalar::all(0),thickness,8);
	
	fontScale = 0.55;
	thickness = 1;
	
	textOrigin.x = 12;
	textOrigin.y = 23;
	
	putText(frameTracking,text.str(),textOrigin,font,fontScale,Scalar::all(255),thickness,8);
	
	text.str("");
	text.clear();
	
	text << "PTracking";
	
	if (agentId != -1) text << " - " << agentId;
	
	text << " (press 'q' to exit, press 'ESC' to restart tracking)";
	
	namedWindow(text.str());
	//moveWindow(text.str(),0,screenHeight - frameTracking.rows - 25);
	
	imshow(text.str(),frameTracking);
	
	if (pTracker.areDataCalibrated())
	{
		/// Global estimations.
		for (MultiAgentEstimations::const_iterator it = globalEstimations.begin(); it != globalEstimations.end(); ++it)
		{
			PTracking::Point2f a;
			cv::Point2f p;
			float angle;
			int arrowMagnitude;
			
			p.x = it->second.first.first.observation.x / pTracker.getResolution();
			p.y = it->second.first.first.observation.y / pTracker.getResolution();
			
			const map<int,pair<int,pair<int,int> > >::const_iterator& colorTrack = colorMap.find(it->first);
			
			fontScale = 0.5;
			thickness = 3;
			arrowMagnitude = 15;
			
			if (it->first < 9)
			{
				textOrigin.x = p.x - 6;
				textOrigin.y = p.y  - 11;
			}
			else
			{
				textOrigin.x = p.x - 11;
				textOrigin.y = p.y  - 11;
			}
			
			text.str("");
			text.clear();
			
			text << it->first;
			
			putText(planViewTracking,text.str(),textOrigin,font,fontScale,Scalar::all(0),thickness,8);
			
			fontScale = 0.5;
			thickness = 2;
			
			if (it->first < 9)
			{
				textOrigin.x = p.x - 4;
				textOrigin.y = p.y  - 12;
			}
			else
			{
				textOrigin.x = p.x - 9;
				textOrigin.y = p.y  - 12;
			}
			
			putText(planViewTracking,text.str(),textOrigin,font,fontScale,Scalar::all(255),thickness,8);
			circle(planViewTracking,cv::Point(p.x,p.y),6,cvScalar(colorTrack->second.first,colorTrack->second.second.first,colorTrack->second.second.second),2);
			
			a.x = it->second.first.first.observation.x;
			a.y = it->second.first.first.observation.y;
			
			PTracking::PointWithVelocity b = Utils::estimatedPosition(a,it->second.first.first.model.averagedVelocity,1);
			
			a.x /= pTracker.getResolution();
			a.y /= pTracker.getResolution();
			
			b.pose.x /= pTracker.getResolution();
			b.pose.y /= pTracker.getResolution();
			
			line(planViewTracking,cv::Point(a.x,a.y),cv::Point(b.pose.x,b.pose.y),cvScalar(colorTrack->second.first,colorTrack->second.second.first,colorTrack->second.second.second),3);			
			
			if ((fabs(it->second.first.first.model.averagedVelocity.x) <= 0.05) && (fabs(it->second.first.first.model.averagedVelocity.y) <= 0.05)) continue;
			
			angle = atan2(a.y - b.pose.y,a.x - b.pose.x);
			
			/// Compute the coordinates of the first segment of the arrow.
			a.x = b.pose.x + (arrowMagnitude * cos(angle + M_PI / 4));
			a.y = b.pose.y + (arrowMagnitude * sin(angle + M_PI / 4));
			
			/// Draw the first segment of the arrow.
			line(planViewTracking,cv::Point(a.x,a.y),cv::Point(b.pose.x,b.pose.y),cvScalar(colorTrack->second.first,colorTrack->second.second.first,colorTrack->second.second.second),3);
			
			/// Compute the coordinates of the second segment of the arrow.
			a.x = b.pose.x + (arrowMagnitude * cos(angle - M_PI / 4));
			a.y = b.pose.y + (arrowMagnitude * sin(angle - M_PI / 4));
			
			/// Draw the second segment of the arrow.
			line(planViewTracking,cv::Point(a.x,a.y),cv::Point(b.pose.x,b.pose.y),cvScalar(colorTrack->second.first,colorTrack->second.second.first,colorTrack->second.second.second),3);
		}
		
		namedWindow("Plan View");
		//moveWindow("Plan View",frameTracking.cols + 3,screenHeight - planView.rows - 25);
		
		imshow("Plan View",planViewTracking);
	}
}

void PTaLer::exec(const string& problemFile, const string& filename, const string& backgroundFilename, bool isLiveSensor, bool isPause)
{
	Mat planView;
	BasicDetector* detector;
	BasicDriver* driver = 0;
	ofstream results;
	string nextFrameFilename, resultFile;
	float fps;
	int frameRateCounter, keyboard, sleepTime;
	bool isPauseGiven;
	
	detector = instantiateDetector(frameRate);
	
	if (detector == 0)
	{
		ERR("Impossible to instantiate a detector. Please check the CMakeLists.txt. Exiting..." << endl);
		
		exit(-1);
	}
	
	detector->setDetectorParameters(string(getenv("PTracking_ROOT")) + string("/../config/detector.cfg"),agentId,calibrationDirectory);
	
	if (isLiveSensor)
	{
		driver = instantiateDriver(filename);
		
		if (driver == 0)
		{
			ERR("Impossible to instantiate a driver. Please check the CMakeLists.txt. Exiting..." << endl);
			
			exit(-1);
		}
	}
	
	keyboard = 0;
	
	/// Avoiding compilation warning.
	if (system("mkdir -p ../results/tracker"));
	
	resultFile = string("../results/tracker/PTracker-") + problemFile + string(".xml");
	
	results.open(resultFile.c_str());
	
	if (results.is_open())
	{
		results << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
		results << "<dataset>" << endl;
	}
	
	/// Loading frame representing the plan view of the current data set.
	if (pTracker.areDataCalibrated())
	{
		planView = imread(calibrationDirectory + string("/PlanView.png"));
		
		if (planView.empty())
		{
			ERR("Impossible to load plan view '");
			INFO(calibrationDirectory << string("/PlanView.png"));
			ERR("'. Please check the problem. Exiting..." << endl);
			
			exit(-1);
		}
	}
	
	isPauseGiven = isPause;
	
	do
	{
		/// An initial background has been given as an initial guess. We reload it at each iteration, so that every experiment starts with the same background.
		if (backgroundFilename != "")
		{
			INFO(endl << "Loading background..." << flush);
			
			detector->loadBg(backgroundFilename);
			
			INFO("done." << endl);
		}
		
		nextFrameFilename = filename;
		
		/// Deleting all the previous estimations.
		pTracker.reset();
		
		fps = 0.0;
		frameRateCounter = 0;
		isPause = isPauseGiven;
		
		do
		{
			const pair<string,Mat>& frame = readFrame(nextFrameFilename,isLiveSensor,driver);
			
			if (!isLiveSensor)
			{
				/// No data available.
				if (frame.first == "EndData") break;
				
				nextFrameFilename = frame.first;
			}
			
			Timestamp initialTimestamp;
			
			const ObjectSensorReading& detections = detector->exec(frame.second);
			
			if (strcasecmp(detectorType.c_str(),"Imbs") == 0)
			{
				if (!detector->isBackgroundCreated())
				{
					static bool isFirstTime = true;
					
					if (isFirstTime)
					{
						INFO(endl << "Generating background..." << flush);
						
						isFirstTime = false;
					}
					
					if (strcasecmp(filename.c_str(),"Kinect") == 0) usleep(30e3);
					
					continue;
				}
				else if (backgroundFilename == "")
				{
					static bool isFirstTime = true;
					
					if (isFirstTime)
					{
						INFO("done." << endl);
						
						isFirstTime = false;
					}
				}
			}
			
			pTracker.exec(detections,&results);
			
			draw(frame.second,pTracker.getAgentEstimations().second,fps,string(frame.first).substr(string(frame.first).rfind('_') + 1,string(frame.first).rfind('.') - string(frame.first).rfind('_') - 1),
				 planView,pTracker.getAgentEstimations().first);
			
			sleepTime = (1000 / frameRate) - (Timestamp() - initialTimestamp).getMs();
			
			if (sleepTime < 1) sleepTime = 1;
			
			if (isPause)
			{
				INFO("Press any key to continue..." << endl);
				
				keyboard = waitKey(0);
				
				isPause = false;
			}
			else
			{
				keyboard = waitKey(sleepTime);
				
				fps = ((fps * frameRateCounter) + (1000.0 / (Timestamp() - initialTimestamp).getMs())) / (frameRateCounter + 1);
				
				++frameRateCounter;
			}
			
			if ((frameRateCounter % 100) == 0)
			{
				pTracker.updateTimeToWaitBeforeDeleting(fps);
				pTracker.updateTimeToWaitBeforePromoting(fps);
			}
		}
		while (((char) keyboard != 27) && ((char) keyboard != 'q'));
		
		if ((strcasecmp(detectorType.c_str(),"Imbs") != 0) || detector->isBackgroundCreated())
		{
			if (results.is_open())
			{
				results << "</dataset>" << endl;
				results.close();
				
				INFO(endl << "Results has been saved in ");
				WARN(resultFile);
				INFO("." << endl);
			}
			
			if ((char) keyboard == 'q') break;
		}
	}
	while ((char) keyboard != 'q');
	
	delete detector;
	delete driver;
}

void PTaLer::execDataset(const string& problemFile, const string& observationFile)
{
	pTracker.exec(observationFile);
}

BasicDetector* PTaLer::instantiateDetector(float frameRate) const
{
	if (strcasecmp(detectorType.c_str(),"Imbs") == 0)
	{
#ifdef Imbs_FOUND
		return new Imbs(frameRate);
#else
		return 0;
#endif
	}
	else if (strcasecmp(detectorType.c_str(),"HOG") == 0)
	{
		return new HOG(frameRate);
	}
	
	ERR("Invalid detector type: '");
	INFO(detectorType);
	ERR("'. ");
	
	return 0;
}

BasicDriver* PTaLer::instantiateDriver(const string& sensorType) const
{
	if (strcasecmp(sensorType.c_str(),"Camera") == 0) return new Camera(agentId);
	else if (strcasecmp(sensorType.c_str(),"Xtion") == 0)
	{
#ifdef OpenNI2_FOUND
		return new Xtion();
#else
		ERR("OpenNI2 library has not been found. ");
		
		return 0;
#endif
	}
	else if (strcasecmp(sensorType.c_str(),"Kinect") == 0)
	{
#ifdef Freenect_FOUND
		return new Kinect();
#else
		ERR("Freenect library has not been found. ");
		
		return 0;
#endif
	}
	
	ERR("Invalid sensor type: '");
	INFO(sensorType);
	ERR("'. ");
	
	return 0;
}

pair<string,Mat> PTaLer::readFrame(const string& filename, bool isLiveSensor, BasicDriver* driver)
{
	if (!isLiveSensor)
	{
		static bool isFirstTime = true;
		
		Mat frame;
		string extension, frameBaseName, frameDigits, nextFrameFilename;
		int failsCounter, frameNumber;
		bool quit, ok;
		
		frame = imread(filename);
		
		if (isFirstTime)
		{
			if (frame.empty())
			{
				ERR("Invalid image name: '");
				INFO(filename);
				ERR("'. Exiting..." << endl);
				
				exit(-1);
			}
			
			isFirstTime = false;
		}
		
		try
		{
			frameBaseName = string(filename).substr(0,string(filename).rfind('_') + 1);
			frameDigits = string(filename).substr(string(filename).rfind('_') + 1,string(filename).rfind('.') - string(filename).rfind('_') - 1);
			extension = string(filename).substr(string(filename).rfind('.'));
		}
		catch (...)
		{
			ERR("Invalid image name: '");
			INFO(filename);
			ERR("'. Exiting..." << endl);
			
			exit(-1);
		}
		
		frameNumber = atoi(frameDigits.c_str());
		
		/// A new image has been read correctly.
		if (!frame.empty())
		{
			stringstream nextFrameStream, s;
			
			s << (++frameNumber);
			
			nextFrameStream << setw(frameDigits.size()) << setfill('0') << s.str();
			
			nextFrameFilename = frameBaseName + nextFrameStream.str() + extension;
			
			return make_pair(nextFrameFilename,frame);
		}
		
		failsCounter = 0;
		quit = false;
		ok = false;
		
		while (!ok)
		{
			stringstream nextFrameStream, s;
			
			s << (++frameNumber);
			
			nextFrameStream << setw(frameDigits.size()) << setfill('0') << s.str();
			
			nextFrameFilename = frameBaseName + nextFrameStream.str() + extension;
			
			frame = imread(nextFrameFilename);
			
			if (frame.empty())
			{
				++failsCounter;
				
				if (failsCounter == MAXIMUM_NUMBER_OF_MISSING_FRAMES)
				{
					quit = true;
					
					break;
				}
				
				ok = false;
			}
			else ok = true;
		}
		
		if (quit) return make_pair("EndData",Mat());
		else return make_pair(nextFrameFilename,frame);
	}
	else
	{
		const Mat& frame = driver->getData();
		
		return make_pair("",frame);
	}
}
