/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <map>
#include <stdexcept>
#include <string>

namespace manfield
{
	template<class P> class PrototypeFactory
	{
		public:
			static std::map<std::string, const P*>& getPrototypeDb();
			
			PrototypeFactory(const P* prototype) throw (std::runtime_error);
			
			static P* forName(const std::string& name) throw (std::runtime_error);
			static void freePrototypeDb();
	};
	
	#define DECLARE_PROTOTYPE(Faktory, PrototypeKlass) \
		manfield::PrototypeFactory<Faktory> factory_##PrototypeKlass(new PrototypeKlass());
	
	#define DECLARE_PROTOTYPE_SUPPORT(Faktory) \
		virtual Faktory* clone() const { return 0; }; \
		##define DEFAULT_CLONE(Klass) \
		virtual Faktory* clone() const { return new Klass(*this); }
	
	#include "PrototypeHandler.hpp"
}
