/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Manfield/PrototypeHandler.h>
#include <Manfield/SensorModel.h>
#include <ThirdParty/GMapping/sensor/sensor_base/sensorreading.h>
#include <ThirdParty/GMapping/utils/macro_params.h>
#include <map>
#include <string>
#include <vector>

namespace manfield
{
	class SensorFilter
	{
		protected:
			SensorModel* m_sensorModel;
			bool ownSensorModel;
			
		public:
			SensorFilter(const std::string& type = "undefined", const std::string& name = "unnamed", bool ownSM = false);
			virtual ~SensorFilter();
			
			virtual SensorFilter* clone() const { return 0; }
			virtual void configure(const std::string& filename) = 0;
			const GMapping::Sensor* getSensor() const { return m_sensorModel->getSensor(); }
			SensorModel* getSensorModel() { return m_sensorModel; }
			virtual void initFromReadings(const std::vector<GMapping::SensorReading*>&) {}
			virtual void initFromUniform() = 0;
			virtual void observe(const GMapping::SensorReading* reading) = 0;
			virtual void predict(const PTracking::PointWithVelocity& oldOdometry, const PTracking::PointWithVelocity& newOdometry) = 0;
			void setSensorModel(SensorModel* sm) { m_sensorModel = sm; }
			inline std::string toString() const { return m_type; }
			
			#define FILTER_DEFAULT_CLONE(Klass) \
				virtual SensorFilter* clone() const { return new Klass(*this); }
			
			PARAM_GET(std::string,type,private,public)
			PARAM_SET_GET(std::string,name,private,public,public)
	};
	
	#define SENSORFILTER_FACTORY(Klass) \
		DECLARE_PROTOTYPE(manfield::SensorFilter, Klass)
}
