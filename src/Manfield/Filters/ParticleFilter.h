/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Core/Filters/ObjectSensorReading.h>
#include <Manfield/SensorFilter.h>
#include <Manfield/Filters/GMLocalizer/Localizer.h>
#include <Utils/Point2f.h>
#include <Utils/Point2of.h>
#include <Utils/PriorityBuffer.h>

namespace manfield
{
	class ParticleFilter : public SensorFilter, public Localizer
	{
		protected:
			std::vector<std::pair<PoseParticleVector,PTracking::Point2of> > clusters;
			
		public:
			ParticleFilter(const std::string& type = "particle", const std::string& name = "unnamed_pf") : SensorFilter(type,name), Localizer(0) {;}
			
			virtual ~ParticleFilter() {;}
			
			virtual void configure(const std::string&);
			std::vector<std::pair<PoseParticleVector,PTracking::Point2of> >& getClusters() { return clusters; }
			SensorModel* getSensorModel() { return m_sensorModel; }
			virtual void initFromUniform();
			
			virtual void observe(const GMapping::SensorReading* reading)
			{
				resetWeight();
				
				if (Localizer::observe(*m_sensorModel,reading)) resample();
			}
			
			virtual void predict(const PTracking::PointWithVelocity&, const PTracking::PointWithVelocity&) {;}
			
			FILTER_DEFAULT_CLONE(ParticleFilter)
	};
}
