/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include "Structs.h"
#include <Manfield/SensorModel.h>
#include <ThirdParty/GMapping/sensor/sensor_range/rangereading.h>
#include <ThirdParty/GMapping/particlefilter/pf.h>
#include <ThirdParty/GMapping/utils/macro_params.h>
#include <cmath>
#include <list>
#include <vector>

class Localizer
{
	protected:
		LocalizerParameters m_params;
		
	public:
		Localizer(unsigned int particles = 1, unsigned int pointSkip = 1);
		virtual ~Localizer() {;}
		
		const LocalizerParameters& getparams() { return m_params; }
		inline PoseParticleVector& getparticles() { return m_params.m_particles; }
		inline uint getparticleNumber() { return m_params.m_particleNumber; }
		
		virtual bool initFromMap(const manfield::SensorModelMap& map);
		virtual bool initFromMap(const manfield::SensorModel& sm);
		virtual void initFromSamples(const PoseParticleVector& particles);
		virtual void initUniform(float xmin,float xmax,float ymin,float ymax);
		virtual bool observe(const manfield::SensorModel& model, const GMapping::SensorReading* observation);
		virtual void resample();
		void resetWeight();
		
		inline void setlikelihoodSigma(float likelihoodSigma) { m_params.m_likelihoodSigma = likelihoodSigma; }
		
		inline void setMotionCalibParameters(float xx, float xy, float xt, float yx, float yy, float yt, float tx, float ty, float tt)
		{
			m_params.xx = xx;
			m_params.xy = xy;
			m_params.xt = xt;
			m_params.yx = yx;
			m_params.yy = yy;
			m_params.yt = yt;
			m_params.tx = tx;
			m_params.ty = ty;
			m_params.tt = tt;
		}
		
		inline void setMotionParameters(float r0, float t0, float rr, float  tr, float rt, float tt)
		{
			m_params.sr0 = r0;
			m_params.st0 = t0;
			m_params.srr = rr;
			m_params.str = tr;
			m_params.srt = rt;
			m_params.stt = tt;
		}
		
		inline void setparticleNumber(uint particleNumber)
		{
			m_params.m_particleNumber = particleNumber;
			m_params.m_particles.resize(m_params.m_particleNumber);
		}
		
		inline void setpointSkip(unsigned int pointSkip) { m_params.m_pointSkip = pointSkip; }
		
		inline void setunknownValue(int unknownValue) { m_params.m_unknownValue = unknownValue; }
};
