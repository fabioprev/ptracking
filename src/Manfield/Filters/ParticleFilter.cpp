/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include <Manfield/ConfigFile/ConfigFile.h>
#include <Manfield/Filters/ParticleFilter.h>
#include <Manfield/Utils/DebugUtils.h>
#include <Utils/Utils.h>
#include <fstream>

using namespace std;
using GMapping::ConfigFile;
using PTracking::Point2f;
using PTracking::Point2of;
using PTracking::PriorityBuffer;
using PTracking::Utils;

namespace manfield
{
	void ParticleFilter::configure(const string& filename)
	{
		ConfigFile fCfg;
		string filterName = "", key, section;
		
		if (!fCfg.read(filename))
		{
			ERR("Error reading file '" << filename << "' for filter configuration. Exiting..." << endl);
			
			exit(-1);
		}
		
		section = "parameters";
		
		try
		{
			key = "filtername";
			filterName = string(fCfg.value(section, key));
		}
		catch (...)
		{
			ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
			
			exit(-1);
		}
		
		setname(filterName);
		
		try
		{
			key = "nparticles";
			int nparticles = fCfg.value(section,key);
			
			Localizer::setparticleNumber(nparticles);
		}
		catch (...)
		{
			ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
			
			exit(-1);
		}
		
		float st0 = 0, sr0 = 0;
		
		try
		{
			key = "st0";
			st0 = fCfg.value(section,key);
			
			key = "sr0";
			sr0 = fCfg.value(section,key);
		}
		catch (...)
		{
			ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
			
			exit(-1);
		}
		
		Localizer::setMotionParameters(sr0,st0,0.0,0.0,0.0,0.0);
		
		key = "posetype";
		
		try
		{
			m_params.setposeType(LocalizerParameters::string2PoseType(fCfg.value(section,key)));
		}
		catch (...)
		{
			ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
			
			exit(-1);
		}
		
		Point2f localizedsigma;
		
		try
		{
			key = "localizedsigmaX";
			localizedsigma.x = fCfg.value(section,key);
			
			key = "localizedsigmaY";
			localizedsigma.y = fCfg.value(section,key);
			
			m_params.setlocalizedSigma(localizedsigma);
		}
		catch (...)
		{
			ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
			
			exit(-1);
		}
	}
	
	void ParticleFilter::initFromUniform()
	{
		if (m_sensorModel == 0)
		{
			cerr << "SensorModel is not initiliazed for filter '" << gettype().c_str() << "'." << endl;
			
			return;
		}
		
		initFromMap(*m_sensorModel);
	}
	
	SENSORFILTER_FACTORY(ParticleFilter)
}
